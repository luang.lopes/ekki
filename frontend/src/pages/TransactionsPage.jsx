import React from "react";
import { Link } from 'react-router-dom';

import TransactionsList from "../components/Transactions/TransactionsList";
import { Col } from "react-grid-system";
import { Button } from "@blueprintjs/core";
import { IconNames } from "@blueprintjs/icons";

const rowStyle = { display: "flex", flexWrap: "wrap" };

function TransactionsPage() {
  return (
    <div className="container" style={{ padding: "16px 0" }}>
      <div style={rowStyle}>
        <Col style={{ padding: "0 8px 16px 8px" }}>
          <Link to="/transactions/new">
            <Button
              icon={IconNames.EXCHANGE}
              intent="primary"
              text="Nova Transação"
            />
          </Link>
        </Col>
      </div>

      <TransactionsList pageSize={10} page={1} />
    </div>
  );
}

export default TransactionsPage;
