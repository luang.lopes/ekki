import React from "react";

import Highlights from "../components/Home/Highlights";
import Welcome from "../components/Home/Welcome";

export default function HomePage() {
  return (
    <div>
      <Welcome />
      <Highlights />
    </div>
  );
}
