import React from "react";
import { Link } from "react-router-dom";
import { Classes, Icon } from "@blueprintjs/core";
import classNames from "classnames";
import { IconNames } from "@blueprintjs/icons";

const wrapperStyle = {
  height: "100%",
  width: "100%",
  paddingTop: "50px",
  display: "flex",
  alignItems: "center",
  justifyContent: "center",
  flexDirection: "column",
  padding: "12px"
};

const textWrapperStyle = {
  display: "flex",
  flexDirection: "row",
  alignItems: "start",
  marginBottom: '16px'
};

const iconMarginStyle = { marginRight: "8px" };

function NotFound(props) {
  return (
    <div style={wrapperStyle}>
      <div style={textWrapperStyle}>
        <Icon
          icon={IconNames.LIFESAVER}
          iconSize={30}
          style={{ ...iconMarginStyle, paddingTop: "8px" }}
        />
        <div>
          <h1 style={{ margin: "0", marginBottom: "8px" }}>
            Página Não Encontrada
          </h1>
          <span className={classNames(Classes.TEXT_MUTED, Classes.TEXT_SMALL)}>
            Não foi possível encontrar a página que você solicitou, verifique o
            endereço e tente novamente.
          </span>
        </div>
      </div>
      <Link
        to="/"
        className={classNames(
          Classes.BUTTON,
          Classes.INTENT_PRIMARY,
          Classes.MINIMAL
        )}
      >
        <Icon icon={IconNames.HOME} style={iconMarginStyle} />
        Voltar a tela inicial
      </Link>
    </div>
  );
}

export default NotFound;
