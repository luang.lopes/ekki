import React from "react";
import { Link } from "react-router-dom";
import { Card, Elevation, Button } from "@blueprintjs/core";
import { Col } from "react-grid-system";
import BalanceViewer from "../components/Dashboard/BalanceViewer";
import AccountInfoViewer from "../components/Dashboard/AccountInfo";
import CreditCardsList from "../components/Dashboard/CreditCardsList";
import FavoredList from "../components/Dashboard/FavoredList";
import TransactionsList from "../components/Transactions/TransactionsList";
import { IconNames } from "@blueprintjs/icons";

const cardStyle = {
  height: "100%",
  margin: "0 8px",
  marginBottom: "16px",
  padding: "16px"
};

const rowStyle = { display: "flex", flexWrap: "wrap" };

const colSizes = { xs: 12, lg: 4 };

export default function DashboardPage() {
  return (
    <div className="container" style={{ padding: "16px 0" }}>
      <div style={rowStyle}>
        <Col style={{ padding: "0 8px 16px 8px" }}>
          <Link to="/transactions/new">
            <Button
              icon={IconNames.EXCHANGE}
              intent="primary"
              text="Nova Transação"
            />
          </Link>
        </Col>
      </div>
      <div style={rowStyle}>
        <Col {...colSizes} style={{ padding: "0 0 16px 0" }}>
          <Card elevation={Elevation.TWO} style={cardStyle}>
            <BalanceViewer />
          </Card>
        </Col>
        <Col {...colSizes} style={{ padding: "0 0 16px 0" }}>
          <Card elevation={Elevation.TWO} style={cardStyle}>
            <AccountInfoViewer />
          </Card>
        </Col>
        <Col {...colSizes} style={{ padding: "0 0 16px 0" }}>
          <Card elevation={Elevation.TWO} style={cardStyle}>
            <CreditCardsList />
          </Card>
        </Col>
      </div>
      <div style={rowStyle}>
        <Col xs={12} lg={6}>
          <div style={{ padding: "8px" }}>
            <TransactionsList hidePagination={true} title="Últimas Transações" page={1} />
            <div style={{ display: 'flex', justifyContent: 'center', padding: '16px' }}>
              <Link to="/transactions">
                <Button intent="primary" text="Ver Todas" />
              </Link>
            </div>
          </div>
        </Col>
        <Col xs={12} lg={6}>
          <div style={{ padding: "8px" }}>
            <FavoredList />
          </div>
        </Col>
      </div>
    </div>
  );
}
