import React, { useState, useContext } from "react";
import { withRouter } from "react-router";
import TransactionForm from "../components/Transactions/TransactionForm";
import { Col } from "react-grid-system";
import {
  Card,
  Collapse,
  Button,
  Intent,
  Alert,
  Dialog,
  Elevation,
  Spinner
} from "@blueprintjs/core";

import { AppContext } from "../context/appContext";
import { AppToaster } from "../shared/AppToaster";
import { accountService } from "../services/accountService";
import TransactionInfo from "../components/Transactions/TransactionInfo";
import PaymentMethod from "../components/Transactions/PaymentMethod";
import PasswordConfirmForm from "../components/Auth/PasswordConfirmForm";
import { transactionService } from "../services/transactionService";
import { IconNames } from "@blueprintjs/icons";

const rowStyle = { display: "flex", flexWrap: "wrap", marginBottom: "16px" };

function NewTransactionPage(props) {
  const [loading, setLoading] = useState(false);

  const [askAddFavoredOpen, setAskAddFavoredOpen] = useState(false);

  const [currentStep, setCurrentStep] = useState(0);

  const [transactionData, setTransactionData] = useState({});

  const [confirmPasswordOpen, setConfirmPasswordOpen] = useState(false);

  const { auth } = useContext(AppContext);

  const intl = new Intl.NumberFormat("pt-br", {
    style: "currency",
    currency: "BRL"
  });

  async function handleTransactionForm(value) {
    if (
      parseInt(value.targetAccountNumber, 10) ===
      parseInt(auth.account.number, 10)
    ) {
      AppToaster.show({
        intent: Intent.DANGER,
        message: "Informe o número de outra conta como destinatário"
      });

      return;
    }

    setLoading(true);

    try {
      const data = await accountService.getAccountDetails(
        value.targetAccountNumber
      );

      setTransactionData({ amount: value.amount, ...data });

      setCurrentStep(1);
    } catch (error) {
      console.log(error);
      AppToaster.show({
        intent: Intent.DANGER,
        message:
          error && error.message ? error.message : "Erro realizar transação"
      });
    } finally {
      setLoading(false);
    }
  }

  async function makeTransaction(data) {
    setLoading(true);

    const toSend = {
      sourceAccountNumber: auth.account.number,
      targetAccountNumber: data.account.number,
      amount: data.amount,
      creditCardNumber: data.paymentMethod.card
        ? data.paymentMethod.card.number
        : undefined,
      password: data.password
    };

    try {
      await transactionService.makeTransaction(toSend);

      AppToaster.show({
        intent: Intent.SUCCESS,
        message: "Transação realizada com sucesso"
      });

      const alreadyFavored = (await accountService.getFavoreds({
        id: transactionData.account.number
      })).favoreds;

      if (alreadyFavored.length === 0) {
        setAskAddFavoredOpen(true);
      } else {
        props.history.replace("/transactions");
      }
    } catch (error) {
      console.log(error);
      AppToaster.show({
        intent: Intent.DANGER,
        message:
          error && error.message ? error.message : "Erro realizar transação"
      });
    } finally {
      setLoading(false);
    }
  }

  async function addFavored() {
    setLoading(true);

    try {
      await accountService.addFavored(
        transactionData.account.number
      );

      AppToaster.show({
        intent: Intent.SUCCESS,
        message: "Favorecido adicionado a lista"
      });
    } catch (error) {
      AppToaster.show({
        intent: Intent.DANGER,
        message:
          error && error.message
            ? error.message
            : "Erro ao adicionar favorecido"
      });
    } finally {
      setLoading(false);

      props.history.replace("/transactions");
    }
  }

  return (
    <div className="container">
      <Alert
        cancelButtonText="Não"
        confirmButtonText="Sim"
        intent="success"
        icon={IconNames.NEW_PERSON}
        isOpen={askAddFavoredOpen}
        onCancel={() => {
          props.history.replace("/transactions");
        }}
        onConfirm={addFavored}
      >
        {loading ? (
          <Spinner intent="primary" />
        ) : (
          <>
            Deseja adicionar{" "}
            <b>
              {transactionData.account && transactionData.account.owner
                ? transactionData.account.owner.name
                : ""}{" "}
            </b>
            a lista de favorecidos?
          </>
        )}
      </Alert>
      <Dialog
        title="Confimração de Senha"
        isOpen={confirmPasswordOpen}
        isCloseButtonShown={false}
      >
        <div style={{ padding: "16px" }}>
          <PasswordConfirmForm
            onSubmit={data => {
              setTransactionData({
                ...transactionData,
                password: data.password
              });

              setConfirmPasswordOpen(false);

              makeTransaction({
                ...transactionData,
                password: data.password
              });
            }}
          />
        </div>
      </Dialog>
      <div style={rowStyle}>
        <Col>
          <h2 style={{ textAlign: "center" }}>Nova Transação</h2>
        </Col>
      </div>
      <div style={rowStyle}>
        <Col>
          <Card elevation={Elevation.TWO}>
            <h2 style={{ textAlign: "center", margin: "0" }}>Dados Gerias</h2>
            <Collapse isOpen={currentStep === 0} keepChildrenMounted={true}>
              <TransactionForm
                loading={loading}
                onSubmit={handleTransactionForm}
              />
            </Collapse>
          </Card>
        </Col>
      </div>
      <div style={rowStyle}>
        <Col>
          <Card elevation={Elevation.TWO}>
            <h2 style={{ textAlign: "center", margin: "0 0 16px 0" }}>
              Forma de Pagamento
            </h2>
            <Collapse isOpen={currentStep === 1}>
              <span style={{ marginBottom: "16px", display: "block" }}>
                <b>Método de pagamento: </b>
                {transactionData.paymentMethod
                  ? transactionData.paymentMethod.type === "credit-card"
                    ? `Cartão de Crédito (${
                        transactionData.paymentMethod.card.number
                      })`
                    : "Débito"
                  : "Não selecionado"}
              </span>
              <PaymentMethod
                amount={transactionData.amount}
                onSelect={evt => {
                  setTransactionData({
                    ...transactionData,
                    paymentMethod: evt
                  });
                }}
              />
              <div style={{ display: "flex", justifyContent: "flex-end" }}>
                <Button
                  text="Voltar"
                  style={{ marginRight: "8px" }}
                  onClick={() => {
                    setTransactionData({
                      ...transactionData,
                      paymentMethod: undefined
                    });
                    setCurrentStep(currentStep - 1);
                  }}
                />
                <Button
                  text="Confirmar"
                  intent="success"
                  onClick={() => setCurrentStep(currentStep + 1)}
                  disabled={!transactionData.paymentMethod}
                />
              </div>
            </Collapse>
          </Card>
        </Col>
      </div>
      <div>
        <Col>
          <Card elevation={Elevation.TWO}>
            <h2 style={{ textAlign: "center", margin: "0" }}>Verificação</h2>
            <Collapse isOpen={currentStep === 2}>
              <div>
                <b>Saldo atual:</b> {intl.format(auth.account.balance)}
                <TransactionInfo
                  amount={transactionData.amount}
                  targetAccount={transactionData.account}
                  card={
                    transactionData.paymentMethod
                      ? transactionData.paymentMethod.card
                      : undefined
                  }
                />
              </div>
              <div style={{ display: "flex", justifyContent: "flex-end" }}>
                <Button
                  text="Voltar"
                  style={{ marginRight: "8px" }}
                  onClick={() => setCurrentStep(currentStep - 1)}
                />
                <Button
                  text="Confirmar"
                  intent="success"
                  onClick={() => {
                    if (transactionData.amount > 1000) {
                      setConfirmPasswordOpen(true);
                    } else {
                      makeTransaction(transactionData);
                    }
                  }}
                />
              </div>
            </Collapse>
          </Card>
        </Col>
      </div>
    </div>
  );
}

export default withRouter(NewTransactionPage);
