import React, { useState, useContext } from "react";
import { Colors, Classes } from "@blueprintjs/core";

import { AppContext } from "../context/appContext";
import { AppToaster } from "../shared/AppToaster";
import { accountService } from "../services/accountService";
import RegisterForm from "../components/Auth/RegisterForm";

const formWrapperStyle = {
  borderRadius: "6px",
  backgroundColor: Colors.LIGHT_GRAY4,
  padding: "16px 10px",
  margin: "24px 16px"
};

function RegisterPage() {
  const { auth } = useContext(AppContext);

  const [isLoading, setIsLoading] = useState(false);

  const [error, setErrors] = useState(null);

  const createAccount = async accountData => {
    setIsLoading(true);

    setErrors(null);

    try {
      const data = await accountService.createAccount(accountData);

      setIsLoading(false);

      auth.login(data.token, data.account);
    } catch (error) {
      const message =
        error && error.message
          ? error.message
          : "Erro ao tentar criar conta. tente novamente mais tarde";

      if (error && error.errors) {
        setErrors(error.errors);
      }

      AppToaster.show({ intent: "danger", message: message });

      setIsLoading(false);
    }
  };

  const onSubmit = data => {
    createAccount({
      password: data.password,
      owner: {
        name: data.name,
        email: data.email,
        cpf: data.cpf
      }
    });
  };

  return (
    <div>
      <div
        style={{
          backgroundColor: Colors.TURQUOISE2,
          display: "flex",
          justifyContent: "center"
        }}
        className={Classes.DARK}
      >
        <h1>Criar Conta</h1>
      </div>
      <div className="container">
        <div style={formWrapperStyle}>
          <RegisterForm onSubmit={onSubmit} loading={isLoading} error={error} />
        </div>
      </div>
    </div>
  );
}

export default RegisterPage;
