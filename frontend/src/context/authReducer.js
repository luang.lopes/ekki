export const authActions = {
  LOGIN: "LOGIN",
  LOGOUT: "LOGOUT",
  SET_BALANCE: "SET_BALANCE"
};

const login = (token, account) => {
  localStorage.setItem("token", token);

  return { token, account };
};

const logout = () => {
  localStorage.removeItem("token");

  return { token: null, account: null };
};

export const authReducer = (state, action) => {
  switch (action.type) {
    case authActions.LOGIN:
      return login(action.token, action.account);
    case authActions.LOGOUT:
      return logout();
    case authActions.SET_BALANCE:
      return {
        ...state,
        account: { ...state.account, balance: action.balance }
      };
    default:
      return state;
  }
};
