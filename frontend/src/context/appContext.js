import React from "react";

export const AppContext = React.createContext({
  auth: {
    token: null,
    account: null,
    login: (token, account) => {},
    logout: () => {},
    setBalance: (balance) => {}
  }
});
