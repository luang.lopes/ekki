import React, { useReducer } from "react";
import { withRouter } from "react-router";
import PropTypes from "prop-types";

import { AppContext } from "./appContext";
import { authActions, authReducer } from "./authReducer";

function GlobalContext(props) {
  const [authContext, dispatch] = useReducer(authReducer, {
    token: null,
    account: null
  });

  const login = (token, account) => {
    dispatch({ type: authActions.LOGIN, token, account });

    props.history.replace("/dashboard");
  };

  const logout = () => {
    dispatch({ type: authActions.LOGOUT });

    props.history.replace("/");
  };

  const setBalance = (balance) => {
    dispatch({ type: authActions.SET_BALANCE, balance })
  }

  return (
    <AppContext.Provider
      value={{
        auth: {
          token: authContext.token,
          account: authContext.account,
          login,
          logout,
          setBalance
        }
      }}
    >
      {props.children}
    </AppContext.Provider>
  );
}

GlobalContext.propTypes = {
  match: PropTypes.object.isRequired,
  location: PropTypes.object.isRequired,
  history: PropTypes.object.isRequired
};

export default withRouter(GlobalContext);
