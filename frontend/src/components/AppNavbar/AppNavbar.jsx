import React, { useContext } from "react";
import { Link } from "react-router-dom";
import { Alignment, Navbar, Icon, Button, Colors } from "@blueprintjs/core";
import { IconNames } from "@blueprintjs/icons";

import { AppContext } from "../../context/appContext";
import AppUserInfo from "./AppUserInfo";
import AppLogin from "../Auth/AppLogin";

const logoStyle = {
  display: "flex",
  alignItems: "center",
  color: Colors.LIGHT_GRAY5
};

function AppNavbar() {
  const context = useContext(AppContext);

  return (
    <nav>
      <Navbar className="bp3-dark" fixedToTop>
        <div className="container">
          <Navbar.Group align={Alignment.LEFT}>
            <Navbar.Heading>
              <Link to="/">
                <h3 style={logoStyle}>
                  <Icon icon={IconNames.DOLLAR} iconSize={20} />
                  Ekki
                </h3>
              </Link>
            </Navbar.Heading>
          </Navbar.Group>
          <Navbar.Group align={Alignment.RIGHT}>
            {context.auth.account !== null ? (
              <AppUserInfo
                user={context.auth.account.owner}
                logout={context.auth.logout}
              />
            ) : (
              <React.Fragment>
                <Link to="/register" style={{ marginRight: "8px" }}>
                  <Button
                    text="Criar Conta"
                    icon={IconNames.PLUS}
                    intent="success"
                  />
                </Link>
                <AppLogin />
              </React.Fragment>
            )}
          </Navbar.Group>
        </div>
      </Navbar>
    </nav>
  );
}

export default AppNavbar;
