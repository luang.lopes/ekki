import React, { useContext } from "react";
import PropTypes from "prop-types";
import { Button, Popover, Classes, Icon } from "@blueprintjs/core";
import { IconNames } from "@blueprintjs/icons";
import classNames from "classnames";

import { AppContext } from '../../context/appContext';

const userInfoStyle = {
  display: "flex",
  flexDirection: "column",
  alignItems: "center",
  padding: "12px",
  justifyContent: "center"
};

function AppUserInfo(props) {
  const {auth} = useContext(AppContext);

  return (
    <Popover>
      <Button minimal>
        <span style={{ display: "flex", alignItems: "center" }}>
          <span style={{ marginRight: "8px" }} className={Classes.TEXT_LARGE}>
            {props.user.name}
          </span>

          <Icon icon={IconNames.USER} iconSize={25} />
        </span>
      </Button>

      <div style={userInfoStyle}>
        <Icon icon={IconNames.USER} iconSize={45} />

        <h3 style={{ margin: "4px 0" }}>{props.user.name}</h3>

        <div style={{ marginBottom: "8px" }}>
          <span className={classNames(Classes.TEXT_MUTED, Classes.TEXT_SMALL)}>
            Conta: {`${auth.account.number}`.padStart(4, '0')}
          </span>
        </div>

        <div>
          <Button text="Sair" intent="danger" onClick={props.logout} />
        </div>
      </div>
    </Popover>
  );
}

AppUserInfo.propTypes = {
  user: PropTypes.object.isRequired,
  logout: PropTypes.func.isRequired
};

export default AppUserInfo;
