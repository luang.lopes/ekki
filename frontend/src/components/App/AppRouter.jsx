import React, { useEffect, useState, useContext } from "react";
import { Route, Switch, Redirect } from "react-router-dom";
import axios from "axios";

import { AppToaster } from "../../shared/AppToaster";
import { AppContext } from "../../context/appContext";

import HomePage from "../../pages/HomePage";
import RegisterPage from "../../pages/RegisterPage";
import NotFound from "../../pages/NotFound";
import DashboardPage from "../../pages/DashboardPage";
import TransactionsPage from "../../pages/TransactionsPage";
import NewTransactionPage from "../../pages/NewTransactionPage";

function PrivateRoute({ component: Component, ...rest }) {
  const { auth } = useContext(AppContext);

  return (
    <Route
      {...rest}
      render={props =>
        auth.account ? (
          <Component {...props} />
        ) : (
          <Redirect to={{ pathname: "/", state: { from: props.location } }} />
        )
      }
    />
  );
}

function GuestRoute({ component: Component, ...rest }) {
  const { auth } = useContext(AppContext);

  return (
    <Route
      {...rest}
      render={props =>
        !auth.account ? (
          <Component {...props} />
        ) : (
          <Redirect
            to={{ pathname: "/dashboard", state: { from: props.location } }}
          />
        )
      }
    />
  );
}

export default function AppRouter(props) {
  const [loading, setLoading] = useState(true);

  const { auth } = useContext(AppContext);

  useEffect(() => {
    verifyAuth();
  }, []);

  async function verifyAuth() {
    try {
      const data = (await axios.get("/api/auth/verify")).data;

      if (data.valid) {
        auth.login(data.token, data.account);
      } else {
        auth.logout();
      }
    } catch (error) {
      console.log(error);
      AppToaster.show({
        intent: "danger",
        message: "Erro verificando autenticação"
      });
      auth.logout();
    } finally {
      setLoading(false);
    }
  }

  return (
    <div {...props}>
      {loading ? (
        "Carregando..."
      ) : (
        <Switch>
          <GuestRoute path="/" exact component={HomePage} />
          <GuestRoute path="/register" exact component={RegisterPage} />
          <PrivateRoute path="/dashboard" exact component={DashboardPage} />
          <PrivateRoute path="/transactions" exact component={TransactionsPage} />
          <PrivateRoute path="/transactions/new" exact component={NewTransactionPage} />
          <Route component={NotFound} />
        </Switch>
      )}
    </div>
  );
}
