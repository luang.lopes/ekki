import React from "react";
import { BrowserRouter } from "react-router-dom";
import { Colors } from "@blueprintjs/core";

import "./App.css";
import AppRouter from "./AppRouter";
import AppNavbar from "../AppNavbar/AppNavbar";
import GlobalContext from "../../context/GlobalContext";

const mainStyle = {
  minHeight: "calc(100vh - 50px)",
  width: "100%",
  marginTop: "50px",
  display: "flex",
  flexDirection: "column"
};

const footerStyle = {
  width: "100%",
  color: Colors.LIGHT_GRAY5,
  background: Colors.DARK_GRAY5,
  padding: "16px 12px"
};

function App() {
  return (
    <BrowserRouter>
      <GlobalContext>
        <div>
          <AppNavbar />
          <main style={mainStyle}>
            <AppRouter style={{ width: "100%", flex: "1" }} />
            <footer style={footerStyle}>
              <div
                className="container"
                style={{
                  color: Colors.LIGHT_GRAY1,
                  display: "flex",
                  justifyContent: "space-between",
                  width: "100%"
                }}
              >
                <span>2019 - Luan G Lopes</span>
                <span>Versão: 1.0.0</span>
              </div>
            </footer>
          </main>
        </div>
      </GlobalContext>
    </BrowserRouter>
  );
}

export default App;
