import React from "react";
import { Divider, Colors } from "@blueprintjs/core";
import { IconNames } from "@blueprintjs/icons";

import HighlightCard from "./HighlightCard";

const wrapperStyle = {
  padding: "16px 0",
  display: "flex",
  flexWrap: "wrap"
};

function Highlights() {
  const highlights = [
    {
      title: "Fácil",
      text: `Com a conta Digital Ekki, em apenas alguns cliques, você envia e
      recebe dinheiro para seus amigos, familiares ou para quem quiser!`,
      icon: IconNames.THUMBS_UP,
      iconColor: Colors.FOREST3
    },
    {
      title: "Rápido",
      text: `Sem filas, sem atrasos, no Ekki o processo é totalmente online,
        você pode ver seu saldo e realizar transações de qualquer lugar
        com seu smartphone, tablet ou computador!`,
      icon: IconNames.FAST_FORWARD,
      iconColor: Colors.COBALT3
    },
    {
      title: "Sem Taxas",
      text: `Chega de pagar para ter seu dinheiro seguro e realizar operações
        online no Ekki sua conta é totalmente isenta de taxas!`,
      icon: IconNames.BANK_ACCOUNT,
      iconColor: Colors.GOLD3
    }
  ];

  const highlightsRender = () => {
    return highlights.map((hl, i) => <HighlightCard key={i} {...hl} />);
  };

  return (
    <div className="container">
      <h2 style={{ textAlign: "center" }}>Destques</h2>
      <Divider />
      <div style={wrapperStyle}>{highlightsRender()}</div>
    </div>
  );
}

export default Highlights;
