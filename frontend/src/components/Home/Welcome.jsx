import React from "react";
import { Link } from "react-router-dom";
import { Colors, Button, Icon, Classes } from "@blueprintjs/core";
import { IconNames } from "@blueprintjs/icons";
import classNames from "classnames";

const wrapperstyle = {
  display: "flex",
  flexDirection: "column",
  justifyContent: "center",
  alignItems: "center",
  padding: "30px 10px"
};

export default function Welcome() {
  return (
    <div
      style={{
        backgroundColor: Colors.TURQUOISE2
      }}
      className={Classes.DARK}
    >
      <div style={wrapperstyle} className="container">
        <h1 style={{ textAlign: "center" }}>
          Bem Vindo ao <Icon icon={IconNames.DOLLAR} iconSize={30} />
          <strong>
            <b>Ekki</b>
          </strong>
        </h1>
        <p
          style={{
            textAlign: "center",
            maxWidth: "600px",
            marginBottom: "32px"
          }}
          className={classNames(Classes.TEXT_LARGE)}
        >
          Um grande banco islandês ouviu seus clientes e agora você pode ter uma
          conta online e sem taxas de forma fácil e rápida!
        </p>
        <Link to="/register">
          <Button
            text="Criar Conta"
            large
            icon={IconNames.PLUS}
            intent="success"
          />
        </Link>
      </div>
    </div>
  );
}
