import React from "react";
import PropTypes from "prop-types";
import { Card, Elevation, Icon, Divider, Classes } from "@blueprintjs/core";

const cardStyle = {
  flex: "1",
  margin: "0 16px",
  display: "flex",
  justifyContent: "start",
  alignItems: "center",
  flexDirection: "column",
  minWidth: "285px",
  marginBottom: "16px"
};

function HighlightCard(props) {
  return (
    <Card elevation={Elevation.TWO} style={cardStyle}>
      <Icon icon={props.icon} iconSize={50} color={props.iconColor} />

      <h2>{props.title}</h2>

      <Divider style={{ width: "100%" }} />

      <p
        style={{ textAlign: "center", marginTop: "16px" }}
        className={Classes.RUNNING_TEXT}
      >
        {props.text}
      </p>
    </Card>
  );
}

HighlightCard.propTypes = {
  title: PropTypes.string.isRequired,
  text: PropTypes.string.isRequired,
  icon: PropTypes.string,
  iconColor: PropTypes.string
};

export default HighlightCard;
