import React, { useContext } from "react";
import { Col } from "react-grid-system";
import { Card, Button } from "@blueprintjs/core";

import { AppContext } from "../../context/appContext";
import CreditCardsList from "../Dashboard/CreditCardsList";

function PaymentMethod(props) {
  const { auth } = useContext(AppContext);

  const intl = new Intl.NumberFormat("pt-br", {
    style: "currency",
    currency: "BRL"
  });

  return (
    <div style={{ display: "flex", flexWrap: "wrap" }}>
      <Col xs={12} md={6} style={{ marginBottom: "16px" }}>
        <Card style={{ height: "100%" }}>
          <div
            style={{
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
              flexDirection: "column"
            }}
          >
            <span style={{ fontSize: "32px", margin: "16px" }}>
              {intl.format(auth.account.balance)}
            </span>
            {props.amount > auth.account.balance ? (
              "Você não possui saldo suficiente"
            ) : (
              <Button
                text="Pagar com Saldo"
                large
                intent="success"
                onClick={() => props.onSelect({ type: "balance" })}
              />
            )}
          </div>
        </Card>
      </Col>
      <Col xs={12} md={6}  style={{ marginBottom: "16px" }}>
        <Card>
          <div style={{ padding: "16px" }}>
            <CreditCardsList
              selectable={true}
              onSelect={card => {
                props.onSelect({ type: "credit-card", card });
              }}
            />
          </div>
        </Card>
      </Col>
    </div>
  );
}

export default PaymentMethod;
