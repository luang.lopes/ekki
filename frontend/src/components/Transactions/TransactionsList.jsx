import React, { useState, useEffect, useContext } from "react";
import PropTypes from "prop-types";

import { AppContext } from "../../context/appContext";
import AppTable from "../../shared/components/AppTable";
import { Icon, Spinner, Intent, Divider } from "@blueprintjs/core";
import { IconNames } from "@blueprintjs/icons";
import { transactionService } from "../../services/transactionService";
import { AppToaster } from "../../shared/AppToaster";
import Pagination from "../../shared/components/Pagination";

function TransactionsList(props) {
  const [transactions, setTransactions] = useState([]);

  const [pagination, setPagination] = useState({
    currentPage: props.page || 1,
    pageSize: props.pageSize || 5,
    totalItems: 0
  });

  const [loading, setLoading] = useState(false);

  const { auth } = useContext(AppContext);

  const intlCurrency = new Intl.NumberFormat("pt-br", {
    style: "currency",
    currency: "BRL"
  });

  const intlDate = new Intl.DateTimeFormat("pt-br", {
    year: "numeric",
    month: "numeric",
    day: "numeric",
    hour: "numeric",
    minute: "numeric",
    hour12: false
  });

  useEffect(() => {
    loadTransactions();
  }, [pagination.currentPage]);

  async function loadTransactions() {
    setLoading(true);
    try {
      const data = await transactionService.getAll({
        limit: pagination.pageSize,
        offset: (pagination.currentPage - 1) * pagination.pageSize
      });

      setTransactions(data.transactions);

      setPagination({ ...pagination, totalItems: data.count });
    } catch (error) {
      console.log(error);
      AppToaster.show({
        intent: Intent.DANGER,
        message: "Erro ao buscar transações"
      });
    } finally {
      setLoading(false);
    }
  }

  const renderRows = () => {
    return transactions.map(tr => (
      <tr key={tr.id}>
        <td>{intlDate.format(new Date(tr.createdAt))}</td>
        <td>{intlCurrency.format(tr.amount)}</td>
        <td style={{ width: "30px" }}>
          {tr.sourceAccountNumber === auth.account.number ? (
            <Icon icon={IconNames.ARROW_DOWN} iconSize={12} intent="danger" />
          ) : (
            <Icon icon={IconNames.ARROW_UP} iconSize={12} intent="success" />
          )}
        </td>
      </tr>
    ));
  };

  return (
    <>
      <h3 className="bp3-heading" style={{ textAlign: "center" }}>
        {props.title ? props.title : "Transações"}
      </h3>
      <Divider />
      {loading ? (
        <Spinner intent={Intent.PRIMARY} size={40} />
      ) : transactions.length === 0 ? (
        <h4 style={{ textAlign: "center" }}>Nenhuma transação</h4>
      ) : (
        <AppTable maxHeight={props.maxHeight}>
          <thead>
            <tr>
              <th>Data e Hora</th>
              <th>Valor</th>
            </tr>
          </thead>
          <tbody>{renderRows()}</tbody>
        </AppTable>
      )}
      {!props.hidePagination ? (
        <div
          style={{
            display: "flex",
            justifyContent: "center",
            padding: "16px 0"
          }}
        >
          <Pagination
            totalItems={pagination.totalItems}
            pageSize={pagination.pageSize}
            page={pagination.currentPage}
            onChangePage={page =>
              setPagination({ ...pagination, currentPage: page })
            }
          />
        </div>
      ) : (
        undefined
      )}
    </>
  );
}

TransactionsList.propTypes = {
  page: PropTypes.number.isRequired
};

export default TransactionsList;
