import React from "react";
import { Formik } from "formik";
import { IconNames } from "@blueprintjs/icons";
import * as Yup from "yup";

import FormFiled from "../../shared/components/FormFiled";
import { Button } from "@blueprintjs/core";

const rowStyle = {
  marginBottom: "10px"
};

function TransactionForm(props) {
  const initialValue = {
    amount: "",
    targetAccountNumber: ""
  };

  const validationSchema = Yup.object().shape({
    amount: Yup.number("Informe apenas números").required(
      "Valor é obrigatório"
    ),
    targetAccountNumber: Yup.number("Informe apenas números").required(
      "Conta de destino é obrigatório"
    )
  });

  const form = ({
    handleChange,
    handleBlur,
    errors,
    values,
    handleSubmit,
    touched,
    setTouched
  }) => (
    <form onSubmit={handleSubmit} autoComplete="off">
      <div style={rowStyle}>
        <FormFiled
          fieldName="targetAccountNumber"
          label="Número da conta do destinatário"
          placeholder="Número da conta do destinatário"
          leftIcon={IconNames.NUMERICAL}
          errors={errors}
          touched={touched}
          values={values}
          onChange={handleChange}
          onBlur={handleBlur}
          required
        />
      </div>
      <div style={rowStyle}>
        <FormFiled
          fieldName="amount"
          label="Valor"
          placeholder="Valor"
          leftIcon={IconNames.DOLLAR}
          type="number"
          errors={errors}
          touched={touched}
          values={values}
          onChange={handleChange}
          onBlur={handleBlur}
          required
        />
      </div>

      <div style={{ display: 'flex', justifyContent: 'flex-end' }}>
        <Button
          loading={props.loading}
          text="Confirmar"
          type="submit"
          icon={IconNames.TICK_CIRCLE}
          onClick={setTouched}
          intent="success"
        />
      </div>
    </form>
  );

  return (
    <Formik
      initialValues={initialValue}
      validationSchema={validationSchema}
      onSubmit={props.onSubmit}
      render={form}
    />
  );
}

export default TransactionForm;
