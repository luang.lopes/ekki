import React from "react";
import * as cpf from "@fnando/cpf";

const rowStyle = { margin: "16px 0" };

function TransactionInfo(props) {
  const intl = new Intl.NumberFormat("pt-br", {
    style: "currency",
    currency: "BRL"
  });

  return (
    <div>
      {props.card ? (
        <div style={rowStyle}>
          <b>Cartão: </b> {props.card.number}
        </div>
      ) : (
        ""
      )}
      <div style={rowStyle}>
        <b>Valor:</b> {intl.format(props.amount)}
      </div>
      <div style={rowStyle}>
        <b>Número da conta do destinatário:</b>{" "}
        {`${props.targetAccount.number}`.padStart(4, "0")}
      </div>
      <div style={rowStyle}>
        <b>Nome do destinatário:</b> {props.targetAccount.owner.name}
      </div>
      <div style={rowStyle}>
        <b>CPF do destinatário:</b>{" "}
        {cpf.format(`${props.targetAccount.owner.cpf}`.padStart(11, "0"))}
      </div>
    </div>
  );
}

export default TransactionInfo;
