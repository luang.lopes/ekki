import React, { useContext } from "react";
import { Button } from "@blueprintjs/core";
import { IconNames } from "@blueprintjs/icons";
import * as Yup from "yup";

import { AppContext } from "../../context/appContext";
import FormFiled from "../../shared/components/FormFiled";
import { Formik } from "formik";

const rowStyle = {
  marginBottom: "10px"
};

function PasswordConfirmForm(props) {
  const { auth } = useContext(AppContext);

  const initialValue = {
    password: ""
  };

  const validationSchema = Yup.object().shape({
    password: Yup.string()
      .trim()
      .required("Senha é obrigatório")
  });

  const form = ({
    handleChange,
    handleBlur,
    errors,
    values,
    handleSubmit,
    touched,
    setTouched
  }) => (
    <form onSubmit={handleSubmit} autoComplete="off">
      <div style={rowStyle}>
        <FormFiled
          fieldName="accountNumber"
          label="Número da conta"
          placeholder="Número da conta"
          leftIcon={IconNames.NUMERICAL}
          errors={errors}
          touched={touched}
          values={{ accountNumber: `${auth.account.number}`.padStart(4, '0') }}
          onChange={handleChange}
          onBlur={handleBlur}
          disabled
          readOnly
        />
      </div>
      <div style={rowStyle}>
        <FormFiled
          fieldName="password"
          label="Senha"
          placeholder="Senha"
          leftIcon={IconNames.LOCK}
          type="password"
          errors={errors}
          touched={touched}
          values={values}
          onChange={handleChange}
          onBlur={handleBlur}
          required
        />
      </div>

      <div style={{ display: "flex", justifyContent: "flex-end" }}>
        <Button
          loading={props.loading}
          text="Confirmar"
          type="submit"
          icon={IconNames.TICK}
          onClick={setTouched}
          intent="success"
        />
      </div>
    </form>
  );

  return (
    <Formik
      initialValues={initialValue}
      validationSchema={validationSchema}
      onSubmit={props.onSubmit}
      render={form}
    />
  );
}

export default PasswordConfirmForm;
