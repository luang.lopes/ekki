import React from "react";
import PropTypes from "prop-types";
import { Button } from "@blueprintjs/core";
import { IconNames } from "@blueprintjs/icons";
import { Formik } from "formik";
import * as Yup from "yup";

import FormFiled from "../../shared/components/FormFiled";

const rowStyle = {
  marginBottom: "10px"
};

function LoginForm(props) {
  const initialValue = {
    accountNumber: "",
    password: ""
  };

  const validationSchema = Yup.object().shape({
    accountNumber: Yup.number().required("Número da conta é obrigatório"),
    password: Yup.string()
      .trim()
      .required("Senha é obrigatório")
  });

  const form = ({
    handleChange,
    handleBlur,
    errors,
    values,
    handleSubmit,
    touched,
    setTouched
  }) => (
    <form onSubmit={handleSubmit} autoComplete="off">
      <div style={rowStyle}>
        <FormFiled
          fieldName="accountNumber"
          label="Número da conta"
          placeholder="Número da conta"
          leftIcon={IconNames.NUMERICAL}
          errors={errors}
          touched={touched}
          values={values}
          onChange={handleChange}
          onBlur={handleBlur}
          required
        />
      </div>
      <div style={rowStyle}>
        <FormFiled
          fieldName="password"
          label="Senha"
          placeholder="Senha"
          leftIcon={IconNames.LOCK}
          type="password"
          errors={errors}
          touched={touched}
          values={values}
          onChange={handleChange}
          onBlur={handleBlur}
          required
        />
      </div>

      <Button
        loading={props.loading}
        text="Entrar"
        type="submit"
        icon={IconNames.LOG_IN}
        fill
        onClick={setTouched}
        intent="success"
      />
    </form>
  );

  return (
    <Formik
      initialValues={initialValue}
      validationSchema={validationSchema}
      onSubmit={props.onSubmit}
      render={form}
    />
  );
}

LoginForm.propTypes = {
  onSubmit: PropTypes.func.isRequired,
  error: PropTypes.shape({
    message: PropTypes.string
  }),
  loading: PropTypes.bool
};

export default LoginForm;
