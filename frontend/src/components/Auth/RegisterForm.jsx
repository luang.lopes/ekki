/* eslint-disable no-template-curly-in-string */
import React, { useEffect } from "react";
import PropTypes from "prop-types";
import { Button, Classes, Colors } from "@blueprintjs/core";
import { Formik } from "formik";
import * as Yup from "yup";
import { IconNames } from "@blueprintjs/icons";

import FormFiled from "../../shared/components/FormFiled";

const formFieldStyle = {
  minWidth: "200px",
  padding: "0 8px"
};

const formSectionStyle = { display: "flex", flexWrap: "wrap" };

function RegisterForm(props) {
  let formErrorSetter;

  useEffect(() => {
    if (formErrorSetter && props.error) {
      formErrorSetter(props.error);
    }
  }, [props.error]);

  const initialValue = {
    name: "",
    cpf: "",
    email: "",
    password: "",
    passwordConfirm: ""
  };

  const validationSchema = Yup.object().shape({
    name: Yup.string()
      .trim()
      .required("Nome é obrigatório"),
    cpf: Yup.string()
      .trim()
      .required("CPF é obrigatório"),
    email: Yup.string()
      .trim()
      .email("Informe um email válido")
      .required("Email é obrigatório"),
    password: Yup.string()
      .trim()
      .min(6, "A senha deve conter ao menos ${min} caracteres")
      .required("Senha é obrigatório"),
    passwordConfirm: Yup.string()
      .trim()
      .min(6, "A senha deve conter ao menos ${min} carateres")
      .oneOf([Yup.ref("password")], "Confirmação de senha incorreta")
      .required("Confirme a senha"),
    agreement: Yup.bool()
      .oneOf([true], "Aceite os termos do contrato")
      .required("Aceite os termos do contrato")
  });

  const form = ({
    handleChange,
    handleBlur,
    errors,
    values,
    handleSubmit,
    touched,
    setTouched,
    setErrors
  }) => {
    if (!formErrorSetter) {
      formErrorSetter = setErrors;
    }

    return (
      <form onSubmit={handleSubmit} autoComplete="off">
        <div style={formSectionStyle}>
          <div style={{ ...formFieldStyle }}>
            <label
              className="bp3-control bp3-checkbox"
              style={{ display: "inline" }}
            >
              <input
                id="agreement"
                type="checkbox"
                value={values.agreement}
                onChange={handleChange}
                onBlur={handleBlur}
              />
              <span className="bp3-control-indicator" />
              <p style={{ display: "inline" }}>
                Estou ciente que este é um app de demostração, a conta criada
                bem como valores e operações são apenas para fins
                demonstrativos, e quero continuar.
              </p>
              <div
                className={Classes.FORM_HELPER_TEXT}
                style={{ color: Colors.RED2, marginTop: "6px" }}
              >
                {errors.agreement}
              </div>
            </label>
          </div>
        </div>
        <div style={formSectionStyle}>
          <div style={{ ...formFieldStyle, width: "50%" }}>
            <FormFiled
              fieldName="name"
              label="Nome"
              placeholder="Nome completo"
              leftIcon={IconNames.USER}
              errors={errors}
              touched={touched}
              values={values}
              onChange={handleChange}
              onBlur={handleBlur}
              required
            />
          </div>
          <div style={{ ...formFieldStyle, width: "50%" }}>
            <FormFiled
              fieldName="cpf"
              label="CPF"
              placeholder="CPF apenas números"
              leftIcon={IconNames.NUMERICAL}
              errors={errors}
              touched={touched}
              values={values}
              onChange={handleChange}
              onBlur={handleBlur}
              required
              type="number"
            />
          </div>
          <div style={{ ...formFieldStyle, width: "100%" }}>
            <FormFiled
              fieldName="email"
              label="Email"
              placeholder="usuario@exemplo.com"
              leftIcon={IconNames.ENVELOPE}
              errors={errors}
              touched={touched}
              values={values}
              onChange={handleChange}
              onBlur={handleBlur}
              required
            />
          </div>
        </div>
        <div style={formSectionStyle}>
          <div style={{ ...formFieldStyle, width: "50%" }}>
            <FormFiled
              fieldName="password"
              label="Senha"
              placeholder="Senha"
              leftIcon={IconNames.LOCK}
              type="password"
              errors={errors}
              touched={touched}
              values={values}
              onChange={handleChange}
              onBlur={handleBlur}
              required
            />
          </div>
          <div style={{ ...formFieldStyle, width: "50%" }}>
            <FormFiled
              fieldName="passwordConfirm"
              label="Confirmação de Senha"
              placeholder="Confirmção de Senha"
              leftIcon={IconNames.LOCK}
              type="password"
              errors={errors}
              touched={touched}
              values={values}
              onChange={handleChange}
              onBlur={handleBlur}
              required
            />
          </div>
        </div>
        <div
          style={{
            width: "100%",
            padding: "0 8px",
            display: "flex",
            justifyContent: "flex-end"
          }}
        >
          <Button
            text="Confirmar"
            onClick={() => setTouched(initialValue)}
            type="submit"
            intent="success"
            icon={IconNames.TICK}
            loading={props.loading}
          />
        </div>
      </form>
    );
  };

  return (
    <Formik
      initialValues={initialValue}
      validationSchema={validationSchema}
      onSubmit={props.onSubmit}
      render={form}
    />
  );
}

RegisterForm.propTypes = {
  onSubmit: PropTypes.func.isRequired,
  error: PropTypes.shape({
    messgae: PropTypes.string
  }),
  loading: PropTypes.bool
};

export default RegisterForm;
