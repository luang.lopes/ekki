import React, { useState, useContext } from "react";
import { Popover, Button } from "@blueprintjs/core";

import { AppContext } from "../../context/appContext";
import LoginForm from "./LoginForm";
import { AppToaster } from "../../shared/AppToaster";
import { authService } from "../../services/authService";

const titleStyle = {
  textAlign: "center",
  margin: "0",
  marginBottom: "8px"
};

function AppLogin() {
  const [loading, setLoading] = useState(false);

  const { auth } = useContext(AppContext);

  const loginAttempt = async credentials => {
    setLoading(true);

    try {
      const data = await authService.login(
        credentials.accountNumber,
        credentials.password
      );

      setLoading(false);

      auth.login(data.token, data.account);
    } catch (error) {
      const message =
        error && error.message
          ? error.message
          : "Erro ao tentar entrar na conta. tente novamente mais tarde";

      if (error && error.errors) {
        // setErrors(error.errors);
      }

      AppToaster.show({ intent: "danger", message: message });

      setLoading(false);
    }
  };

  return (
    <Popover>
      <Button text="Entrar" minimal intent="primary" />
      <div style={{ padding: "12px", width: "340px" }}>
        <h3 style={titleStyle}>Entrar na Conta</h3>
        <LoginForm loading={loading} onSubmit={loginAttempt} />
      </div>
    </Popover>
  );
}

export default AppLogin;
