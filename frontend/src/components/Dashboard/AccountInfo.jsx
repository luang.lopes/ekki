import React, { useContext } from "react";
import * as cpf from "@fnando/cpf";

import { AppContext } from "../../context/appContext";
import { Divider, Icon } from "@blueprintjs/core";
import { IconNames } from "@blueprintjs/icons";

function AccountInfoViewer() {
  const { auth } = useContext(AppContext);

  return (
    <div>
      <h3 className="bp3-heading">
        <Icon intent="primary" icon={IconNames.INFO_SIGN} iconSize={30} /> Informações
      </h3>
      <Divider />
      <div>
        <span style={{ margin: "8px 0", display: "inline-block" }}>
          <b>Conta: </b>
          {`${auth.account.number}`.padStart(4, "0")}
        </span>
      </div>
      <div>
        <span style={{ margin: "8px 0", display: "inline-block" }}>
          <b>Email: </b>
          {auth.account.owner.email}
        </span>
      </div>
      <div>
        <span style={{ margin: "8px 0", display: "inline-block" }}>
          <b>CPF: </b>
          {cpf.format(`${auth.account.owner.cpf}`.padStart(11, "0"))}
        </span>
      </div>
    </div>
  );
}

export default AccountInfoViewer;
