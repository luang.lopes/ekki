import React, { useState, useEffect } from "react";
import {
  Divider,
  Colors,
  Button,
  Dialog,
  Classes,
  Icon,
  Spinner,
  Intent,
  Alert
} from "@blueprintjs/core";
import { IconNames } from "@blueprintjs/icons";

import { AppToaster } from "../../shared/AppToaster";
import { accountService } from "../../services/accountService";
import CreditCardForm from "./CreditCardForm";
import CreditCardDetail from './CreditCardDetail';
import { creditCardService } from "../../services/cardService";

const cardRowStyle = {
  display: "flex",
  justifyContent: "space-between",
  padding: "6px 0"
};

const cardColStyle = { display: "flex", flexDirection: "column" };

function CardRow({ card, onDeleteClick, onEditClick, onViewClick, onSelect, selectable }) {
  const intl = new Intl.DateTimeFormat("pt-br");

  const maskedNumber = "*".repeat(12) + card.number.substr(-4);

  return (
    <React.Fragment>
      <div
        onClick={() => onSelect(card)}
        style={{ ...cardRowStyle, cursor: selectable ? "pointer" : undefined }}
      >
        <div style={{ ...cardColStyle, flex: "1" }}>
          <span className="bp3-text-muted bp3-text-small">Número</span>
          <span
            className="bp3-text-muted bp3-text-large"
            style={{ color: Colors.BLACK, fontWeight: "bold" }}
          >
            {maskedNumber}
          </span>
        </div>
        <div style={{ ...cardColStyle, marginRight: "16px" }}>
          <span className="bp3-text-muted bp3-text-small">Venc.</span>
          <span
            className="bp3-text-muted bp3-text-large"
            style={{ color: Colors.BLACK }}
          >
            {intl.format(new Date(card.maturity)).substr(3)}
          </span>
        </div>
        <div
          style={{
            ...cardColStyle,
            flexDirection: "row",
            alignItems: "center",
            justifyContent: "center"
          }}
        >
        <Icon
            style={{ cursor: "pointer", marginRight: "6px" }}
            intent="success"
            icon={IconNames.EYE_OPEN}
            iconSize={20}
            onClick={evt => {
              evt.preventDefault();
              evt.stopPropagation();
              onViewClick(card);
            }}
          />
          <Icon
            style={{ cursor: "pointer", marginRight: "6px" }}
            intent="primary"
            icon={IconNames.EDIT}
            iconSize={20}
            onClick={evt => {
              evt.preventDefault();
              evt.stopPropagation();
              onEditClick(card);
            }}
          />
          <Icon
            style={{ cursor: "pointer" }}
            intent="danger"
            icon={IconNames.DELETE}
            iconSize={18}
            onClick={evt => {
              evt.preventDefault();
              evt.stopPropagation();
              onDeleteClick(card);
            }}
          />
        </div>
      </div>
      <Divider />
    </React.Fragment>
  );
}

function CreditCardsList({ selectable, onSelect }) {
  const [cards, setCards] = useState([]);

  const [viewOpen, setViewOpen] = useState(false);

  const [formModalOpen, setFormModalOpen] = useState(false);

  const [selectedCard, setSelectedCard] = useState(null);

  const [deleteModalOpen, setDeleteModalOpen] = useState(null);

  const [loading, setLoading] = useState(true);

  useEffect(() => {
    loadCards();
  }, []);

  const renderCards = () =>
    cards.map(card => (
      <CardRow
        key={card.number}
        card={card}
        selectable={selectable}
        onSelect={onSelect}
        onDeleteClick={card => {
          setSelectedCard(card);
          setDeleteModalOpen(true);
        }}
        onEditClick={card => {
          setSelectedCard(card);
          setFormModalOpen(true);
        }}
        onViewClick={card => {
          setSelectedCard(card);
          setViewOpen(true);
        }}
      />
    ));

  async function loadCards() {
    setLoading(true);
    try {
      const data = await accountService.getCards();

      setCards(data.cards);
    } catch (error) {
      console.log(error);
      AppToaster.show({
        intent: "danger",
        message: "Erro ao carregar cartões"
      });
    } finally {
      setLoading(false);
    }
  }

  async function createCard(cardData) {
    setLoading(true);
    try {
      const data = await creditCardService.create(cardData.limit);

      setCards([data.creditCard, ...cards]);
    } catch (error) {
      console.log(error);
      AppToaster.show({
        intent: "danger",
        message: "Erro ao criar cartão"
      });
    } finally {
      setLoading(false);

      setFormModalOpen(false);
    }
  }

  async function removeCard() {
    setLoading(true);

    const number = selectedCard.number;

    setSelectedCard(null);
    try {
      await creditCardService.delete(number);

      setCards(cards.filter(cr => cr.number !== number));
    } catch (error) {
      console.log(error);
      AppToaster.show({
        intent: "danger",
        message: "Erro ao deletar cartão"
      });
    } finally {
      setLoading(false);
    }
  }

  return (
    <div>
      <Alert isOpen={viewOpen} onConfirm={() => {
        setViewOpen(false);
        setSelectedCard(null)
      }}>
        <CreditCardDetail card={selectedCard} />
      </Alert>

      <Dialog
        isOpen={formModalOpen}
        onClose={() => {
          setFormModalOpen(false);
          setSelectedCard(null);
        }}
        title={`${
          selectedCard ? "Editar limite do" : "Novo"
        } cartão de crédito`}
      >
        <div className={Classes.DIALOG_BODY}>
          <CreditCardForm
            card={selectedCard}
            loading={loading}
            onSubmit={createCard}
          />
        </div>
      </Dialog>

      <Alert
        intent="danger"
        confirmButtonText="Sim"
        cancelButtonText="Não"
        icon={IconNames.TRASH}
        isOpen={deleteModalOpen}
        onCancel={() => {
          setDeleteModalOpen(false);
          setSelectedCard(null);
        }}
        onClose={() => {
          setDeleteModalOpen(false);
          setSelectedCard(null);
        }}
        onConfirm={removeCard}
      >
        Tem certeza que deseja excluir o cartão com número{" "}
        <b>{selectedCard && selectedCard.number}</b> ?
      </Alert>

      <div
        style={{
          display: "flex",
          alignItems: "center",
          justifyContent: "flex-start"
        }}
      >
        <Icon
          color={Colors.ORANGE3}
          icon={IconNames.CREDIT_CARD}
          iconSize={30}
        />
        &nbsp;
        <h3 className="bp3-heading" style={{ margin: "0", flex: "1" }}>
          Cartões
        </h3>
        <Button
          onClick={() => setFormModalOpen(true)}
          icon={IconNames.PLUS}
          intent="primary"
        />
      </div>

      <Divider />

      <div
        style={{
          height: "100%",
          maxHeight: "130px",
          overflow: "auto"
        }}
      >
        {loading ? (
          <Spinner intent={Intent.PRIMARY} size={35} />
        ) : cards.length > 0 ? (
          renderCards()
        ) : (
          <div
            style={{
              display: "flex",
              alignItems: "center",
              flexDirection: "column"
            }}
          >
            <h4>Nenhum Cartão</h4>
            <Button
              text="Adicionar"
              onClick={() => setFormModalOpen(true)}
              icon={IconNames.PLUS}
              intent="primary"
            />
          </div>
        )}
      </div>
    </div>
  );
}

export default CreditCardsList;
