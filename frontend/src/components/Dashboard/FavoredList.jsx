import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import {
  Icon,
  Colors,
  Divider,
  Tooltip,
  Alert,
  Intent,
  Spinner
} from "@blueprintjs/core";
import { IconNames } from "@blueprintjs/icons";
import { accountService } from "../../services/accountService";

import { AppToaster } from "../../shared/AppToaster";
import AppTable from "../../shared/components/AppTable";
import Pagination from "../../shared/components/Pagination";

function DeleteFavoredConfirm(props) {
  return (
    <Alert
      {...props}
      cancelButtonText="Não"
      confirmButtonText="Sim"
      icon="trash"
      intent={Intent.DANGER}
    >
      <p>
        Tem Certeza que deseja excluir <strong>{props.favoredName}</strong> da
        sua lista de favorecidos?
      </p>
    </Alert>
  );
}

DeleteFavoredConfirm.propTypes = {
  favoredName: PropTypes.string
};

function FavoredList(props) {
  const [deleteState, setdeleteState] = useState({
    deleteConfirmOpen: false,
    selectedFavored: null
  });

  const [favoreds, setFavoreds] = useState([]);

  const [pagination, setPagination] = useState({
    currentPage: 1,
    pageSize: 5,
    totalItems: 0
  });

  const [loading, setLoading] = useState(false);

  useEffect(() => {
    loadFavoreds();
  }, [pagination.currentPage]);

  async function loadFavoreds() {
    setLoading(true);
    try {
      const data = await accountService.getFavoreds({
        limit: pagination.pageSize,
        offset: (pagination.currentPage - 1) * pagination.pageSize
      });

      setFavoreds(data.favoreds);
    } catch (error) {
      AppToaster.show({
        intent: Intent.DANGER,
        message: "Erro ao buscar favorecidos"
      });
    } finally {
      setLoading(false);
    }
  }

  async function removeFavored() {
    setLoading(true);

    setdeleteState({ ...deleteState, deleteConfirmOpen: false });

    try {
      await accountService.removeFavored(deleteState.selectedFavored.number);

      setdeleteState({ deleteConfirmOpen: false, selectedFavored: null });

      AppToaster.show({
        intent: Intent.SUCCESS,
        message: "Favorecido removido com sucesso!"
      });
    } catch (error) {
      AppToaster.show({
        intent: Intent.DANGER,
        message: "Erro ao remover favorecido"
      });
    } finally {
      setLoading(false);

      loadFavoreds();
    }
  }

  return (
    <React.Fragment>
      <DeleteFavoredConfirm
        favoredName={
          deleteState.selectedFavored
            ? deleteState.selectedFavored.owner.name
            : ""
        }
        isOpen={deleteState.deleteConfirmOpen}
        onCancel={() =>
          setdeleteState({ deleteConfirmOpen: false, selectedId: null })
        }
        onConfirm={removeFavored}
      />
      <h3 className="bp3-heading" style={{ textAlign: "center" }}>
        Favorecidos
      </h3>
      <Divider />
      {loading ? (
        <Spinner intent={Intent.PRIMARY} size={40} />
      ) : favoreds.length === 0 ? (
        <h4 style={{ textAlign: "center" }}>Nenhum favorecido</h4>
      ) : (
        <AppTable maxHeight="180px">
          <thead>
            <tr>
              <th style={{ textAlign: "center" }}>Nome</th>
              <th style={{ textAlign: "center" }}>Conta</th>
              <th style={{ textAlign: "center" }}>Ações</th>
            </tr>
          </thead>
          <tbody>
            {favoreds.map(fv => (
              <tr key={fv.number}>
                <td>{fv.owner.name}</td>
                <td style={{ textAlign: "center" }}>
                  {`${fv.number}`.padStart(4, "0")}
                </td>
                <td style={{ width: "90px", textAlign: "center" }}>
                  <Tooltip content="Remover">
                    <Icon
                      onClick={() =>
                        setdeleteState({
                          deleteConfirmOpen: true,
                          selectedFavored: fv
                        })
                      }
                      icon={IconNames.DELETE}
                      style={{
                        color: Colors.RED3,
                        marginRight: "12px",
                        cursor: "pointer"
                      }}
                      iconSize={15}
                    />
                  </Tooltip>
                </td>
              </tr>
            ))}
          </tbody>
        </AppTable>
      )}
      <Pagination
        totalItems={pagination.totalItems}
        pageSize={pagination.pageSize}
        page={pagination.currentPage}
        onChangePage={page =>
          setPagination({ ...pagination, currentPage: page })
        }
      />
    </React.Fragment>
  );
}

export default FavoredList;
