import React from "react";

const lineStyle = { padding: '8px 0' }

function CreditCardDetail({ card }) {
  if (!card) {
    return "";
  }

  const intl = new Intl.NumberFormat("pt-br", {
    style: "currency",
    currency: "BRL"
  });

  return (
    <div>
      <div style={lineStyle}>
        <b>Número:</b> {card.number}
      </div>
      <div style={lineStyle}>
        <b>Limite Total:</b> {intl.format(card.limit)}
      </div>
      <div style={lineStyle}>
        <b>Limite utilizado:</b> {intl.format(card.usedLimit)}
      </div>
      <div style={lineStyle}>
        <b>Limite disponivel:</b> {intl.format(card.limit - card.usedLimit)}
      </div>
    </div>
  );
}

export default CreditCardDetail;
