import React from "react";
import PropTypes from "prop-types";
import { Button } from "@blueprintjs/core";
import { IconNames } from "@blueprintjs/icons";
import * as Yup from "yup";

import FormFiled from "../../shared/components/FormFiled";
import { Formik } from "formik";

function CreditCardForm(props) {
  const initialValue = {
    limit: props.card ? props.card.limit : 100.0
  };

  const validationSchema = Yup.object().shape({
    limit: Yup.number().required("Limite é obrigatório")
  });

  const form = ({
    handleChange,
    handleBlur,
    errors,
    values,
    handleSubmit,
    touched,
    setTouched
  }) => (
    <form onSubmit={handleSubmit} autoComplete="off">
      <div>
        <FormFiled
          fieldName="limit"
          label="Limite"
          placeholder="Limite"
          leftIcon={IconNames.DOLLAR}
          type="number"
          step=".10"
          errors={errors}
          touched={touched}
          values={values}
          onChange={handleChange}
          onBlur={handleBlur}
          required
        />
      </div>

      <div style={{ display: "flex", justifyContent: "flex-end" }}>
        <Button
          loading={props.loading}
          text={ props.card ? "Salvar" : "Criar"}
          type="submit"
          icon={props.card ? IconNames.FLOPPY_DISK : IconNames.PLUS}
          onClick={setTouched}
          intent="success"
        />
      </div>
    </form>
  );

  return (
    <>
      {props.card && (
        <span style={{ display: 'block' ,marginBottom: '8px' }}>
          <strong>
            <b>Número: </b>
          </strong>
          {props.card.number}
        </span>
      )}
      <Formik
        initialValues={initialValue}
        validationSchema={validationSchema}
        onSubmit={props.onSubmit}
        render={form}
      />
    </>
  );
}

CreditCardForm.propTypes = {
  onSubmit: PropTypes.func.isRequired,
  loading: PropTypes.bool,
  card: PropTypes.object
};

export default CreditCardForm;
