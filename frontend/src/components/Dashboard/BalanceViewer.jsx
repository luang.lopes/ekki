import React, { useContext, useState, useEffect } from "react";

import { AppContext } from "../../context/appContext";
import { Divider, Icon, Colors, Button } from "@blueprintjs/core";
import { IconNames } from "@blueprintjs/icons";
import { accountService } from "../../services/accountService";
import { AppToaster } from "../../shared/AppToaster";

function BalanceViewer() {
  const [loading, setLoading] = useState(false)

  const { auth } = useContext(AppContext);

  const intl = new Intl.NumberFormat("pt-br", {
    style: "currency",
    currency: "BRL"
  });

  useEffect(() => {
    refreshBalance();
  }, [])

  async function refreshBalance() {
    setLoading(true);

    try {
      const data = await accountService.getBalance();

      auth.setBalance(data.balance);
    } catch (error) {
        AppToaster.show({
          intent: 'danger',
          message: 'Erro ao recarregar saldo'
        })
    } finally {
      setLoading(false);
    }
  }

  return (
    <div>
      <h3 className="bp3-heading">
        <Icon color={Colors.GOLD3} icon={IconNames.DOLLAR} iconSize={30} /> Saldo atual
      </h3>
      <Divider />
      <div>
        <span className={loading ? 'bp3-skeleton' : ''} style={{ fontSize: "32px" }}>
          {intl.format(auth.account.balance)}
        </span>
      </div>
      <div style={{ marginTop: '16px' }}>
        <Button icon={IconNames.REFRESH} onClick={refreshBalance} />
      </div>
    </div>
  );
}

export default BalanceViewer;
