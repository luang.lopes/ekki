import { Toaster, Position } from "@blueprintjs/core";

export const AppToaster = Toaster.create({ position: Position.TOP_RIGHT });
