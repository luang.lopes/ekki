import React from "react";
import PropTypes from 'prop-types'

function AppTable(props) {
  return (
    <table
      style={{ width: "100%", maxHeight: props.maxHeight }}
      className="bp3-html-table bp3-html-table-striped bp3-html-table-condensed"
    >
      {props.children}
    </table>
  );
}

AppTable.propTypes = {
  maxHeight: PropTypes.string
}

export default AppTable;
