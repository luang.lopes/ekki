import React from "react";
import PropTypes from "prop-types";
import { Button, ButtonGroup } from "@blueprintjs/core";
import { IconNames } from "@blueprintjs/icons";

const propTypes = {
  totalItems: PropTypes.number.isRequired,
  onChangePage: PropTypes.func.isRequired,
  page: PropTypes.number,
  pageSize: PropTypes.number
};

const defaultProps = {
  page: 1,
  pageSize: 10
};

class Pagination extends React.Component {
  constructor(props) {
    super(props);
    this.state = { pager: {}, currentPage: 1 };
  }

  componentWillMount() {
    this.setPager(this.props.page);
  }

  componentDidUpdate(prevProps) {
    if(this.props.totalItems !== prevProps.totalItems) {
      this.setPager(this.props.page, true);
    }
  }

  pageChange(page) {
    if (page < 1 || page > this.state.pager.totalPages) {
      return;
    }

    this.setState({ currentPage: page });

    this.props.onChangePage(page);
  }

  setPager(page, force) {
    let { totalItems, pageSize } = this.props;

    let pager = this.state.pager;

    if (!force && (page < 1 || page > pager.totalPages)) {
      return;
    }

    // get new pager object for specified page
    pager = this.getPager(totalItems, page, pageSize);

    // update state
    this.setState({ pager: pager });
  }

  getPager(totalItems, currentPage, pageSize) {
    // default to first page
    currentPage = currentPage || 1;

    // default page size is 10
    pageSize = pageSize || 10;

    // calculate total pages
    let totalPages = Math.ceil(totalItems / pageSize);

    let startPage, endPage;
    if (totalPages <= 10) {
      // less than 10 total pages so show all
      startPage = 1;
      endPage = totalPages;
    } else {
      // more than 10 total pages so calculate start and end pages
      if (currentPage <= 6) {
        startPage = 1;
        endPage = 10;
      } else if (currentPage + 4 >= totalPages) {
        startPage = totalPages - 9;
        endPage = totalPages;
      } else {
        startPage = currentPage - 5;
        endPage = currentPage + 4;
      }
    }

    // calculate start and end item indexes
    let startIndex = (currentPage - 1) * pageSize;
    let endIndex = Math.min(startIndex + pageSize - 1, totalItems - 1);

    // create an array of pages to repeat in the pager control
    let pages = [...Array(endPage + 1 - startPage).keys()].map(
      i => startPage + i
    );

    // return object with all pager properties required by the view
    return {
      totalItems: totalItems,
      currentPage: currentPage,
      pageSize: pageSize,
      totalPages: totalPages,
      startPage: startPage,
      endPage: endPage,
      startIndex: startIndex,
      endIndex: endIndex,
      pages: pages
    };
  }

  render() {
    let pager = this.state.pager;

    let currentPage = this.state.currentPage;

    if (!pager.pages || pager.pages.length <= 1) {
      // don't display pager if there is only 1 page
      return null;
    }

    return (
      <ButtonGroup>
        <Button
          disabled={currentPage === 1}
          onClick={() => this.pageChange(currentPage - 1)}
          icon={IconNames.CARET_LEFT}
        />
        {pager.pages.map((page, index) => (
          <Button
            key={index}
            intent={currentPage === page ? "primary" : "none"}
            onClick={() => this.pageChange(page)}
          >
            {page}
          </Button>
        ))}
        <Button
          disabled={currentPage === pager.totalPages}
          onClick={() => this.pageChange(currentPage + 1)}
          icon={IconNames.CARET_RIGHT}
        />
      </ButtonGroup>
    );
  }
}

Pagination.propTypes = propTypes;
Pagination.defaultProps = defaultProps;
export default Pagination;
