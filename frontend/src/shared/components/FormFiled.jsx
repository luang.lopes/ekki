import React from "react";
import PropTypes from "prop-types";
import { FormGroup, InputGroup } from "@blueprintjs/core";

function FormFiled({
  errors,
  touched,
  leftIcon,
  values,
  label,
  fieldName,
  required,
  type,
  ...restProps
}) {
  const hasError = errors[fieldName] && touched[fieldName];

  return (
    <FormGroup
      helperText={hasError ? errors[fieldName] : " "}
      intent={hasError ? "danger" : "none"}
      label={label}
      labelFor={fieldName}
      labelInfo={required ? "(obrigatório)" : ""}
    >
      <InputGroup
        leftIcon={leftIcon}
        intent={hasError ? "danger" : "none"}
        value={values[fieldName]}
        id={fieldName}
        name={fieldName}
        type={type}
        {...restProps}
      />
      {!hasError && (
        <span style={{ display: "block", fontSize: "12px" }}>&nbsp;</span>
      )}
    </FormGroup>
  );
}

FormFiled.propTypes = {
  fieldName: PropTypes.string.isRequired,
  values: PropTypes.object.isRequired,
  touched: PropTypes.object.isRequired,
  label: PropTypes.string,
  errors: PropTypes.object,
  leftIcon: PropTypes.string,
  required: PropTypes.bool
};

export default FormFiled;
