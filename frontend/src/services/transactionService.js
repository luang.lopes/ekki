import axios from "axios";

const endpoint = "/api/transactions";

const transactionService = {};

transactionService.makeTransaction = async (transaction) => {
  try {
    const result = await axios.post(endpoint, { transaction });

    return result.data;
  } catch (error) {
    if (error && error.response) throw error.response.data;

    throw error;
  }
};

transactionService.getAll = async (params = {}) => {
  try {
    const result = await axios.get(endpoint + '/by-account', { params });

    return result.data;
  } catch (error) {
    if (error && error.response) throw error.response.data;

    throw error;
  }
}

export { transactionService };
