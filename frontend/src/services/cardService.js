import axios from "axios";

const endpoint = "/api/credit-cards";

const creditCardService = {};

creditCardService.create = async (limit) => {
  try {
    const result = await axios.post(endpoint, { limit });

    return result.data;
  } catch (error) {
    if (error && error.response) throw error.response.data;

    throw error;
  }
};

creditCardService.delete = async (number) => {
  try {
    const result = await axios.delete(`${endpoint}/${number}`);

    return result.data;
  } catch (error) {
    if (error && error.response) throw error.response.data;

    throw error;
  }
}

export { creditCardService };
