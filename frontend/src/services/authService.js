import axios from "axios";

const endpoint = "/api/auth";

const authService = {};

authService.login = async (accountNumber, password) => {
  try {
    const result = await axios.post(endpoint, { accountNumber, password });

    return result.data;
  } catch (error) {
    if (error && error.response) throw error.response.data;

    throw error;
  }
};

export { authService };
