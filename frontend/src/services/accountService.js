import axios from "axios";

const endpoint = "/api/accounts";

const accountService = {};

accountService.createAccount = async accountData => {
  try {
    const result = await axios.post(endpoint, { account: accountData });

    return result.data;
  } catch (error) {
    if (error && error.response) throw error.response.data;

    throw error;
  }
};

accountService.getAccountDetails = async (accountNumber) => {
  try {
    const result = await axios.get(`${endpoint}/${accountNumber}`);

    return result.data;
  } catch (error) {
    if (error && error.response) throw error.response.data;

    throw error;
  }
}

accountService.getCards = async () => {
  try {
    const result = await axios.get(`${endpoint}/credit-cards`);

    return result.data;
  } catch (error) {
    if (error && error.response) throw error.response.data;

    throw error;
  }
};

accountService.getFavoreds = async (params) => {
  try {
    const result = await axios.get(`${endpoint}/favoreds`, { params });

    return result.data;
  } catch (error) {
    if (error && error.response) throw error.response.data;

    throw error;
  }
};

accountService.getBalance = async () => {
  try {
    const result = await axios.get(`${endpoint}/balance`);

    return result.data;
  } catch (error) {
    if (error && error.response) throw error.response.data;

    throw error;
  }
};

accountService.addFavored = async favoredAccountNumber => {
  try {
    const result = await axios.put(`${endpoint}/add-favored`, {
      favoredAccountNumber
    });

    return result.data;
  } catch (error) {
    if (error && error.response) throw error.response.data;

    throw error;
  }
};

accountService.removeFavored = async favoredAccountNumber => {
  try {
    const result = await axios.put(`${endpoint}/remove-favored`, {
      favoredAccountNumber
    });

    return result.data;
  } catch (error) {
    if (error && error.response) throw error.response.data;

    throw error;
  }
};

export { accountService };
