'use strict';

module.exports = {
  up: (queryInterface, { DataTypes }) => {
    return queryInterface.createTable('credit-cards', {
      number: {
        type: DataTypes.STRING,
        allowNull: false,
        primaryKey: true,
      },
      accountNumber: {
        type: DataTypes.INTEGER(4).ZEROFILL,
        references: {
          model: 'accounts',
          key: 'number'
        },
        onDelete: 'cascade'
      },
      limit: {
        type: DataTypes.DOUBLE(10, 2),
        allowNull: false,
      },
      usedLimit: {
        type: DataTypes.DOUBLE(10, 2),
        allowNull: false,
        defaultValue: 0
      },
      maturity: {
        type: DataTypes.DATE,
        allowNull: false
      },
      createdAt: {
        allowNull: false,
        type: DataTypes.DATE
      },
      updatedAt: {
        allowNull: false,
        type: DataTypes.DATE
      }
    });
  },

  down: (queryInterface) => {
    return queryInterface.dropTable('credit-cards');
  }
};
