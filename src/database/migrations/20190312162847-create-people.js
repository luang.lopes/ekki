'use strict';

module.exports = {
  up: (queryInterface, { DataTypes }) => {
    return queryInterface.createTable('people', {
      cpf: {
        primaryKey: true,
        unique: true,
        allowNull: false,
        type: DataTypes.STRING(11)
      },
      name: {
        allowNull: false,
        type: DataTypes.STRING
      },
      email: {
        allowNull: false,
        type: DataTypes.STRING,
        unique: true
      },
      createdAt: {
        allowNull: false,
        type: DataTypes.DATE
      },
      updatedAt: {
        allowNull: false,
        type: DataTypes.DATE
      }
    });
  },

  down: queryInterface => {
    return queryInterface.dropTable('people');
  }
};
