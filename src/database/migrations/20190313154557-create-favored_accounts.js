'use strict';

module.exports = {
  up: (queryInterface, { DataTypes }) => {
    return queryInterface.createTable('favored-accounts', {
      accountNumber: {
        type: DataTypes.INTEGER(4).ZEROFILL,
        primaryKey: 'favoredAccountId',
        references: {
          model: 'accounts',
          key: 'number'
        },
        onDelete: 'cascade'
      },
      favoredAccountNumber: {
        type: DataTypes.INTEGER(4).ZEROFILL,
        primaryKey: 'favoredAccountId',
        references: {
          model: 'accounts',
          key: 'number'
        },
        onDelete: 'cascade'
      },
      createdAt: {
        allowNull: false,
        type: DataTypes.DATE
      },
      updatedAt: {
        allowNull: false,
        type: DataTypes.DATE
      }
    });
  },

  down: queryInterface => {
    return queryInterface.dropTable('favored-accounts');
  }
};
