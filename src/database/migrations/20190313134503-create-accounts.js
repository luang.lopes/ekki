'use strict';

module.exports = {
  up: (queryInterface, { DataTypes }) => {
    return queryInterface.createTable('accounts', {
      number: {
        allowNull: false,
        autoIncrement: true,
        unique: true,
        primaryKey: true,
        type: DataTypes.INTEGER(4).ZEROFILL
      },
      balance: {
        type: DataTypes.DOUBLE(10, 2),
        allowNull: false,
        defaultValue: 0
      },
      password: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      personCpf: {
        type: DataTypes.STRING(11),
        primaryKey: true,
        references: {
          model: 'people',
          key: 'cpf',
          constraint: false
        },
        onDelete: 'cascade'
      },
      createdAt: {
        allowNull: false,
        type: DataTypes.DATE
      },
      updatedAt: {
        allowNull: false,
        type: DataTypes.DATE
      }
    });
  },

  down: queryInterface => {
    return queryInterface.dropTable('accounts');
  }
};
