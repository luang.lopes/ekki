'use strict';

module.exports = {
  up: (queryInterface, DataTypes) => {
    return queryInterface.createTable('transactions', {
      id: {
        allowNull: false,
        autoIncrement: true,
        unique: true,
        primaryKey: true,
        type: DataTypes.INTEGER
      },
      sourceAccountNumber: {
        type: DataTypes.INTEGER(4).ZEROFILL,
        references: {
          model: 'accounts',
          key: 'number'
        },
        onDelete: 'cascade'
      },
      targetAccountNumber: {
        type: DataTypes.INTEGER(4).ZEROFILL,
        references: {
          model: 'accounts',
          key: 'number'
        },
        onDelete: 'cascade'
      },
      amount: {
        type: DataTypes.DOUBLE(10, 2),
        allowNull: false
      },
      canceled: {
        type: DataTypes.BOOLEAN,
        allowNull: false,
        defaultValue: false
      },
      createdAt: {
        allowNull: false,
        type: DataTypes.DATE
      },
      updatedAt: {
        allowNull: false,
        type: DataTypes.DATE
      }
    });
  },

  down: queryInterface => {
    return queryInterface.dropTable('transactions');
  }
};
