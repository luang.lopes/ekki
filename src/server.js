const { app } = require('./app');
const { createDebugger } = require('./debugger');
const http = require('http');

const { debuggerPrd } = createDebugger('server');

const port = normalizePort(process.env.EKKI_PORT | 4000);

const server = http.createServer(app);

/**
 *
 * @param {(string|number)} port
 *
 * @returns {number} Porta com tipo number
 */
function normalizePort(port) {
  if (typeof port === 'string') {
    return parseInt(port);
  }

  return port;
}

server.listen(port);

server.on('listening', () => {
  debuggerPrd(`Servidor Rodando em http://localhost:${port}`);
});

server.on('error', error => {
  if (error.syscall !== 'listen') {
    throw error;
  }

  switch (error.code) {
  case 'EACCES':
    debuggerPrd(port + ' requires elevated privileges');

    process.exit(1);

    break;
  case 'EADDRINUSE':
    debuggerPrd(port + ' is already in use');

    process.exit(1);

    break;
  default:
    throw error;
  }
});
