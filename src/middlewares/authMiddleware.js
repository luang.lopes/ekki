const jwt = require('jsonwebtoken');
const createError = require('http-errors');
const { Account } = require('../models');

module.exports = async (req, _res, next) => {
  const token =
    req.headers.authorization && req.headers.authorization.split(' ')[1];

  try {
    if (!token) {
      throw createError(401, { code: 401, message: 'Não autorizado' });
    }

    const decoded = jwt.verify(token, process.env.EKKI_JWT_SECRET);

    if (!decoded) {
      throw createError(401, { code: 401, message: 'Não autorizado' });
    }

    const accountModel = await Account.findByPk(decoded.accountNumber, { include: ['owner'] });

    if (!accountModel) {
      throw createError(401, { code: 401, message: 'Não autorizado' });
    }

    req.account = accountModel.get();

    return next();
  } catch (error) {
    return next(error);
  }
};
