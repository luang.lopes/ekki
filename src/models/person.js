module.exports = (sequelize, DataTypes) => {
  const Person = sequelize.define(
    'Person',
    {
      name: { type: DataTypes.STRING },
      email: { type: DataTypes.STRING, validate: { isEmail: true } },
      cpf: { type: DataTypes.STRING, primaryKey: true }
    },
    {
      tableName: 'people'
    }
  );

  Person.associate = models => {
    models.Person.hasMany(models.Account, {
      as: 'accounts',
      foreignKey: 'personCpf',
      sourceKey: 'cpf'
    });
  };

  return Person;
};
