module.exports = (sequelize, DataTypes) => {
  const Transaction = sequelize.define('Transaction', {
    amount: { type: DataTypes.DOUBLE(10, 2) },
    canceled: { type: DataTypes.BOOLEAN, defaultValue: false }
  });

  Transaction.associate = models => {
    models.Transaction.belongsTo(models.Account, {
      as: 'sourceAccount',
      foreignKey: 'sourceAccountNumber',
      targetKey: 'number'
    });

    models.Transaction.belongsTo(models.Account, {
      as: 'targetAccount',
      foreignKey: 'targetAccountNumber',
      targetKey: 'number'
    });
  };

  return Transaction;
};
