const bcrypt = require('bcrypt');

module.exports = (sequelize, DataTypes) => {
  const Account = sequelize.define(
    'Account',
    {
      number: {
        type: DataTypes.INTEGER(4).ZEROFILL,
        primaryKey: true,
        autoIncrement: true
      },
      balance: {
        type: DataTypes.DOUBLE(10, 2)
      },
      password: { type: DataTypes.STRING, validate: { len: [6] } }
    },
    {
      defaultScope: {
        attributes: { exclude: ['password'] }
      },
      scopes: {
        withPassword: {
          attibutes: { include: ['password'] }
        }
      }
    }
  );

  // Codifica a senha antes de atualizar/criar a conta
  Account.beforeSave(async account => {
    try {
      if (account.password) {
        const hash = await bcrypt.hash(account.password, 10);

        account.password = hash;
      }
    } catch (error) {
      throw error;
    }
  });

  Account.associate = models => {
    models.Account.hasMany(models.CreditCard, {
      as: 'creditCards',
      foreignKey: 'accountNumber',
      sourceKey: 'number'
    });

    models.Account.belongsTo(models.Person, {
      as: 'owner',
      foreignKey: 'personCpf',
      targetKey: 'cpf'
    });

    models.Account.belongsToMany(models.Account, {
      as: 'favoreds',
      through: 'favored-accounts',
      foreignKey: 'accountNumber',
      otherKey: 'favoredAccountNumber',
      uniqueKey: 'favoredAccountId'
    });
  };

  return Account;
};
