module.exports = (sequelize, DataTypes) => {
  const CreditCard = sequelize.define('CreditCard', {
    number: {
      type: DataTypes.STRING,
      primaryKey: true,
      autoIncrement: true
    },
    limit: {
      type: DataTypes.DOUBLE(10, 2)
    },
    usedLimit: {
      type: DataTypes.DOUBLE(10, 2)
    },
    maturity: {
      type: DataTypes.DATE
    }
  }, {
    tableName: 'credit-cards'
  });

  CreditCard.associate = models => {
    models.CreditCard.belongsTo(models.Account, {
      as: 'account',
      foreignKey: 'accountNumber',
      targetKey: 'number'
    });
  };

  return CreditCard;
};
