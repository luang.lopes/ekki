const dotenv = require('dotenv');
const fs = require('fs');
const path = require('path');

dotenv.config();

const express = require('express');
const appRouter = require('./routes/appRouter');

const app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: false }));

app.use('/api', appRouter, (_req, res) =>
  res.status(404).json({ error: { messgae: 'Endpoint Não Encontrado' } })
);

if (fs.existsSync(path.join(__dirname, '../public'))) {
  app.use('/', express.static(path.join(__dirname, '../public')));

  app.use((_req, res) => {
    res.sendfile(path.join(__dirname, '../public/index.html'));
  });
}

app.use((err, _req, res, _next) => {
  console.log(err);

  return res.status(err.code || 500).json({ ...err });
});

module.exports.app = app;
