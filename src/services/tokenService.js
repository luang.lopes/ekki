const jwt = require('jsonwebtoken');

const tokenService = {};

tokenService.createToken = accountNumber => {
  return new Promise((resolve, reject) => {
    jwt.sign(
      {
        accountNumber
      },
      process.env.EKKI_JWT_SECRET,
      { expiresIn: 60 * 30 }, // token expira em 30 minutos
      (err, token) => {
        if (err) {
          return reject(err);
        }

        resolve(token);
      }
    );
  });
};

tokenService.verifyToken = token => {
  return new Promise((resolve) => {
    jwt.verify(token, process.env.EKKI_JWT_SECRET, (err, decoded) => {
      resolve(decoded);
    });
  });
};

module.exports = tokenService;
