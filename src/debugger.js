const debug = require('debug');

/**
 * Cria debugers de produção e desenvolvimento para o namespace informado
 *
 * @param {string} namespace
 *
 * @returns {object} Objeto com debuger de desenvolvimento e produção para o namespace informado
 */
module.exports.createDebugger = namespace => {
  if (!namespace) {
    throw new Error('"namespace" is required');
  }

  return {
    debuggerDev: debug(`${process.env.EKKI_DEBUG_PREFIX}:D:${namespace}`),
    debuggerPrd: debug(`${process.env.EKKI_DEBUG_PREFIX}:P:${namespace}`)
  };
};
