const { Router } = require('express');
const AuthController = require('../controllers/AuthController');

const path = '/auth';

const middlewares = [];

const router = Router();

router.post('/', async (req, res, next) => {
  const { accountNumber, password } = req.body;

  const authCtrl = new AuthController();

  try {
    const result = await authCtrl.authenticate(accountNumber, password);

    return res.json(result);
  } catch (error) {
    return next(error);
  }
});

router.get('/verify', async (req, res, next) => {
  const token =  req.headers.authorization && req.headers.authorization.split(' ')[1];

  const authCtrl = new AuthController();

  try {
    const result = await authCtrl.verify(token);

    return res.json(result);
  } catch (error) {
    return next(error);
  }
});

module.exports = { path, middlewares, router };
