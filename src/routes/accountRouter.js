const { Router } = require('express');
const AccountController = require('../controllers/AccountController');
const authMiddleware = require('../middlewares/authMiddleware');

const path = '/accounts';

const middlewares = [];

const router = Router();

router.post('/', async (req, res, next) => {
  const { body } = req;

  const accCtrl = new AccountController();

  try {
    const result = await accCtrl.create(body.account);

    return res.json(result);
  } catch (error) {
    return next(error);
  }
});

router.get('/balance', authMiddleware, async (req, res, next) => {
  // Pega numero da conta autenticada na requisição
  // somete pode buscar seu prórpio saldo
  const accountNumber = req.account.number;

  const accCtrl = new AccountController();

  try {
    const balance = await accCtrl.getBalance(accountNumber);

    return res.json({ balance });
  } catch (error) {
    return next(error);
  }
});

router.get('/credit-cards', authMiddleware, async (req, res, next) => {
  const accountNumber = req.account.number;

  const accCtrl = new AccountController();

  try {
    const cards = await accCtrl.getCards(accountNumber);

    return res.json({ cards });
  } catch (error) {
    return next(error);
  }
});

router.get('/favoreds', authMiddleware, async (req, res, next) => {
  const accountNumber = req.account.number;

  const accCtrl = new AccountController();

  try {
    const favoreds = await accCtrl.getFavoreds(accountNumber, req.query);

    return res.json({ favoreds });
  } catch (error) {
    return next(error);
  }
});

router.get('/:accountNumber', async (req, res, next) => {
  const accountNumber = req.params.accountNumber;

  const accCtrl = new AccountController();

  try {
    const account = await accCtrl.getAccountDetails(accountNumber);

    return res.json({ account });
  } catch (error) {
    return next(error);
  }
});

router.put('/add-favored', authMiddleware, async (req, res, next) => {
  const { favoredAccountNumber } = req.body;

  // Pega numero da conta autenticada na requisição
  // somete pode adicionar um favorito a si mesmo
  const accountNumber = req.account.number;

  const accCtrl = new AccountController();

  try {
    const result = await accCtrl.addFavored(
      accountNumber,
      favoredAccountNumber
    );

    return res.json(result);
  } catch (error) {
    return next(error);
  }
});

router.put('/remove-favored', authMiddleware, async (req, res, next) => {
  const { favoredAccountNumber } = req.body;

  // Pega numero da conta autenticada na requisição
  // somete pode remover seus próprios favorecidos
  const accountNumber = req.account.number;

  const accCtrl = new AccountController();

  try {
    const result = await accCtrl.removeFavored(
      accountNumber,
      favoredAccountNumber
    );

    return res.json(result);
  } catch (error) {
    return next(error);
  }
});

module.exports = { path, middlewares, router };
