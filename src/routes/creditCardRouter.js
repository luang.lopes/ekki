const { Router } = require('express');
const CreditCardController = require('../controllers/CreditCardController');
const authMiddleware = require('../middlewares/authMiddleware');

const path = '/credit-cards';

const middlewares = [authMiddleware];

const router = Router();

router.post('/', async (req, res, next) => {
  // Pega o numero da conta autenticada na requisição
  // somente pode adicionar cartões a própria conta
  const accountNumber = req.account.number;

  const { limit } = req.body;

  const cdcCtrl = new CreditCardController();

  try {
    const creditCard = await cdcCtrl.create(limit, accountNumber);

    return res.json({ creditCard });
  } catch (error) {
    return next(error);
  }
});

router.put('/', async (req, res, next) => {
  // Pega o numero da conta autenticada na requisição
  // somente pode alterar o limite dos seus cartões
  const accountNumber = req.account.number;

  const { limit, number } = req.body;

  const cdcCtrl = new CreditCardController();

  try {
    const creditCard = await cdcCtrl.changeLimit(number, limit, accountNumber);

    return res.json({ creditCard });
  } catch (error) {
    return next(error);
  }
});

router.delete('/:number', async (req, res, next) => {
  const { number } = req.params;

  const accountNumber = req.account.number;

  const cdcCtrl = new CreditCardController();

  try {
    const result = await cdcCtrl.delete(number, accountNumber);

    return res.json(result);
  } catch (error) {
    return next(error);
  }
});

module.exports = { path, middlewares, router };
