const { Router } = require('express');
const PersonController = require('../controllers/PersonController');
const authMiddleware = require('../middlewares/authMiddleware');

const path = '/people';

const middlewares = [authMiddleware];

const router = Router();

router.put('/', async (req, res, next) => {
  // Pega o cpf do dono da conta autenticada na requisição
  // somete pode alterar seus próprios dados
  const personCpf = req.account.owner.cpf;

  const { person } = req.body;

  const prsCtrl = new PersonController();

  try {
    const updatedPerson = await prsCtrl.update(personCpf, person);

    return res.json({ person: updatedPerson });
  } catch (error) {
    return next(error);
  }
});

module.exports = { path, middlewares, router };
