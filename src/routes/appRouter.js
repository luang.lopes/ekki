const { readdirSync } = require('fs');
const { Router } = require('express');
const { createDebugger: createDebuger } = require('../debugger');

const { debuggerPrd } = createDebuger('router');

const appRouter = Router();

/**
 * Registra as rotas presentes em src/routes
 */
const routersRegister = () => {
  readdirSync(__dirname).forEach(function(file) {
    // ignora a importação deste arquivo
    if (file !== 'appRouter.js') {
      const { router, middlewares, path } = require('./' + file);

      const mdls = middlewares || [];

      appRouter.use(path, ...mdls, router);
    }
  });

  debuggerPrd('Rotas registradas');
};

routersRegister();

module.exports = appRouter;
