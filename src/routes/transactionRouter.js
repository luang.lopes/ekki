const { Router } = require('express');
const TransactionController = require('../controllers/TransactionController');
const authMiddleware = require('../middlewares/authMiddleware');

const path = '/transactions';

const middlewares = [authMiddleware];

const router = Router();

router.post('/', async (req, res, next) => {
  const { transaction } = req.body;

  const trsCtrl = new TransactionController();

  try {
    const newTransaction = await trsCtrl.makeTransaction(transaction);

    return res.json({ transaction: newTransaction });
  } catch (error) {
    return next(error);
  }
});

router.get('/by-account', async (req, res, next) => {
  const { limit, offset } = req.query;

  const accountNumber = req.account.number;

  const trsCtrl = new TransactionController();

  try {
    const result = await trsCtrl.getTransactionsByAccount(
      accountNumber,
      limit,
      offset
    );

    return res.json(result);
  } catch (error) {
    return next(error);
  }
});

module.exports = { path, middlewares, router };
