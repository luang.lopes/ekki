const createError = require('http-errors');
const generator = require('creditcard-generator');
const { CreditCard } = require('../models');

class CreditCardController {
  generateCardNumber(banner = 'VISA') {
    return new Promise(async (resolve, reject) => {
      try {
        let number = generator.GenCC(banner, 1)[0];

        const alreadyExixts = await CreditCard.count({ where: { number } });

        if (alreadyExixts > 0) {
          number = await this.generateCardNumber(
            banner === 'VISA' ? 'Mastercard' : 'VISA'
          );
        }

        resolve(number);
      } catch (error) {
        reject(error);
      }
    });
  }

  async create(limit, accountNumber) {
    try {
      const number = await this.generateCardNumber();

      const creditCardModel = await CreditCard.create({
        number,
        accountNumber,
        limit,
        maturity: new Date().setFullYear(2050)
      });

      return creditCardModel.get();
    } catch (error) {
      throw error;
    }
  }

  async changeLimit(number, newLimit, accountNumber) {
    try {
      const creditCardModel = await CreditCard.findOne({
        where: { number, accountNumber }
      });

      if (!creditCardModel) {
        throw createError(404, {
          message: 'Cartão não encontrado',
          code: 404
        });
      }

      if (newLimit < creditCardModel.limit - creditCardModel.usedLimit) {
        throw createError(400, {
          message: 'Erro ao atualizar limite',
          code: 400,
          error: { limit: 'Limite abaixo saldo devido' }
        });
      }

      creditCardModel.limit = newLimit;

      await creditCardModel.save();

      return creditCardModel.get();
    } catch (error) {
      throw error;
    }
  }

  async delete(number, accountNumber) {
    try {
      const creditCardModel = await CreditCard.findOne({ where: { number, accountNumber }});

      if (!creditCardModel) {
        throw createError(404, {
          code: 404,
          message: 'Cartão de crédito não encontrado'
        });
      }

      await creditCardModel.destroy();

      return { deleted: true };
    } catch (error) {
      throw error;
    }
  }
}

module.exports = CreditCardController;
