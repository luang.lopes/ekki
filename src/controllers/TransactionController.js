const createError = require('http-errors');
const {
  Account,
  Transaction,
  CreditCard,
  Sequelize,
  sequelize
} = require('../models');
const AuthController = require('./AuthController');

class TransactionController {
  async isDuplicated(transactionData, sqzTransaction) {
    try {
      const date = new Date();

      const duplicatedTransactionModel = await Transaction.findOne({
        where: {
          amount: transactionData.amount,
          sourceAccountNumber: transactionData.sourceAccountNumber,
          targetAccountNumber: transactionData.targetAccountNumber,
          canceled: false,
          createdAt: {
            [Sequelize.Op.gt]: date.setMinutes(date.getMinutes() - 2)
          }
        },
        transaction: sqzTransaction
      });

      if (!duplicatedTransactionModel) {
        return false;
      }

      return duplicatedTransactionModel;
    } catch (error) {
      throw error;
    }
  }

  async makeTransaction(transactionData) {
    try {
      const transaction = await sequelize.transaction(async t => {
        try {
          if (transactionData.amount > 1000) {
            const authController = new AuthController();

            const passwordMatch = await authController.verifyCredentials(
              transactionData.sourceAccountNumber,
              transactionData.password
            );

            if (!passwordMatch) {
              throw createError(400, {
                code: 400,
                message: 'Não é possível realizar a transação, senha incorreta'
              });
            }
          }

          const duplicated = await this.isDuplicated(transactionData, t);

          if (duplicated) {
            await Transaction.update(
              { canceled: true },
              { where: { id: duplicated.id }, transaction: t }
            );

            const transactionModel = await Transaction.create(transactionData, {
              transaction: t
            });

            return transactionModel.get();
          }

          const [sourceAccount, targetAccount] = await Promise.all([
            Account.findOne({
              where: { number: transactionData.sourceAccountNumber },
              transaction: t
            }),
            Account.findOne({
              where: { number: transactionData.targetAccountNumber },
              transaction: t
            })
          ]);

          if (!sourceAccount || !targetAccount) {
            throw createError(400, {
              code: 400,
              message: 'Não é possível concluir a transação'
            });
          }

          let creditCardModel;

          if (transactionData.creditCardNumber) {
            creditCardModel = await CreditCard.findByPk(
              transactionData.creditCardNumber
            );

            if (!creditCardModel) {
              throw createError(400, {
                code: 400,
                message: 'Cartão de crédito não encontrado'
              });
            }

            if (
              transactionData.amount >
              creditCardModel.limit - creditCardModel.usedLimit
            ) {
              throw createError(400, {
                code: 400,
                message:
                  'Limite do cartão insuficiente para completar transação'
              });
            }

            creditCardModel.usedLimit =
              creditCardModel.usedLimit + transactionData.amount;
          } else if (sourceAccount.balance - transactionData.amount < 0) {
            throw createError(400, {
              code: 400,
              message: 'Saldo insuficiente'
            });
          }

          const newSourceBalance =
            sourceAccount.balance - transactionData.amount;

          const newTargetBalance =
            targetAccount.balance + transactionData.amount;

          await Promise.all([
            transactionData.creditCardNumber
              ? creditCardModel.save({ transaction: t })
              : sourceAccount.update(
                { balance: newSourceBalance },
                { transaction: t }
              ),
            targetAccount.update(
              { balance: newTargetBalance },
              { transaction: t }
            )
          ]);

          const transactionModel = await Transaction.create(transactionData, {
            transaction: t
          });

          return transactionModel.get();
        } catch (error) {
          throw error;
        }
      });

      return transaction;
    } catch (error) {
      throw error;
    }
  }

  async getTransactionsByAccount(accountNumber, limit, offset = 0) {
    try {
      const result = await Transaction.findAndCountAll({
        where: {
          [Sequelize.Op.or]: [
            { targetAccountNumber: accountNumber },
            { sourceAccountNumber: accountNumber }
          ],
          canceled: false
        },
        limit: limit ? parseInt(limit, 10) : undefined,
        offset: offset ? parseInt(offset, 10) : undefined,
        order: [['createdAt', 'DESC']]
      });

      return {
        count: result.count,
        transactions: result.rows.map(tr => tr.get())
      };
    } catch (error) {
      throw error;
    }
  }
}

module.exports = TransactionController;
