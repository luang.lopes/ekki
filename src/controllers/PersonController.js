const createError = require('http-errors');
const { createDebugger } = require('../debugger');
const { Person } = require('../models');

const { debuggerDev } = createDebugger('perosnController');

class PersonController {
  async validate(personData, oldData) {
    try {
      let cpfAlreadyExists;

      // Caso seja um update e o cpf seja diferente do atual ou seja uma ctiação
      // verifica se não existe outra pessoa com o cpf
      if (oldData) {
        if (personData.cpf !== oldData.cpf) {
          cpfAlreadyExists = await Person.findByPk(personData.cpf);
        } else {
          cpfAlreadyExists = 0;
        }
      } else {
        cpfAlreadyExists = await Person.count({ cpf: personData.cpf });
      }

      if (cpfAlreadyExists > 0) {
        throw createError(400, {
          code: 400,
          message: 'Erro de validação',
          errors: { cpf: 'CPF já cadastrado para outra pessoa' }
        });
      }

      let emailAlreadyExists;

      // Caso seja um update e o email seja diferente do atual ou seja uma ctiação
      // verifica se não existe outra pessoa com o cpf
      if (oldData) {
        if (personData.email !== oldData.email) {
          emailAlreadyExists = await Person.findOne({
            where: { email: personData.email }
          });
        } else {
          emailAlreadyExists = 0;
        }
      } else {
        emailAlreadyExists = await Person.count({ email: personData.email });
      }

      if (emailAlreadyExists > 0) {
        throw createError(400, {
          code: 400,
          message: 'Erro de validação',
          errors: { email: 'Email já cadastrado para outra pessoa' }
        });
      }
    } catch (error) {
      throw error;
    }
  }

  async update(personCpf, personData) {
    try {
      const personModel = await Person.findByPk(personCpf);

      if (!personModel) {
        throw createError(404, { code: 404, message: 'Pessoa não encontrada' });
      }

      await this.validate(personData, personModel.get());

      await personModel.update(personData);

      return personModel.get();
    } catch (error) {
      throw error;
    }
  }
}

module.exports = PersonController;
