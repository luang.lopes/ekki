const createError = require('http-errors');
const tokenService = require('../services/tokenService');
const { Account } = require('../models');

class AccountController {
  async validate(accountData) {
    try {
      const [cpfHasAccount, emailHasAccount] = await Promise.all([
        Account.count({
          where: { personCpf: accountData.owner.cpf }
        }),
        Account.count({
          include: [
            { association: 'owner', where: { email: accountData.owner.email } }
          ]
        })
      ]);

      if (cpfHasAccount > 0) {
        throw createError(400, {
          message: 'Erro de validação',
          code: 400,
          errors: { cpf: 'Já existe uma conta cadastrada para este CPF' }
        });
      }

      if (emailHasAccount > 0) {
        throw createError(400, {
          message: 'Erro de validação',
          code: 400,
          errors: { email: 'Já existe uma conta cadastrada para este Email' }
        });
      }

      const error = {
        message: 'Erro de validação',
        code: 400,
        errors: {}
      };

      const passwordValid =
        typeof accountData.password === 'string' &&
        accountData.password.length >= 6;

      if (!passwordValid) {
        error.errors.password = 'A senha deve conter pelo menos 6 carateres';
      }

      if (Object.keys(error.errors).length > 0) {
        throw createError(400, error);
      }
    } catch (error) {
      throw error;
    }
  }

  async create(accountData) {
    try {
      await this.validate(accountData);

      let accountModel = await Account.create(accountData, {
        include: ['owner']
      });

      await accountModel.reload({ include: ['owner'] });

      const token = await tokenService.createToken(accountModel.number);

      return { account: accountModel.get(), token };
    } catch (error) {
      throw error;
    }
  }

  async getAccountDetails(accountNumber) {
    try {
      const accountModel = await Account.findByPk(accountNumber, {
        include: ['owner']
      });

      if (!accountModel) {
        throw createError(404, { code: 404, message: 'Conta não encontrada' });
      }

      const accountData = accountModel.get();

      delete accountData.balance;

      return accountData;
    } catch (error) {
      throw error;
    }
  }

  async getCards(accountNumber) {
    try {
      const accountModel = await Account.findByPk(accountNumber, {
        include: ['creditCards']
      });

      if (!accountModel) {
        throw createError(404, { code: 404, message: 'Conta não encontrada' });
      }

      return accountModel.get().creditCards;
    } catch (error) {
      throw error;
    }
  }

  async getFavoreds(accountNumber, params) {
    const { offset, limit, ...searchParams } = params;
    try {
      const accountModel = await Account.findByPk(accountNumber, {
        where: searchParams,
        limit: limit ? parseInt(limit, 10) : undefined,
        offset: offset ? parseInt(offset, 10) : undefined,
        include: [{ association: 'favoreds', include: ['owner'] }]
      });

      if (!accountModel) {
        throw createError(404, { code: 404, message: 'Conta não encontrada' });
      }

      return accountModel.get().favoreds;
    } catch (error) {
      throw error;
    }
  }

  async getBalance(accountNumber) {
    try {
      const accountModel = await Account.findByPk(accountNumber, {
        attributes: ['balance']
      });

      if (!accountModel) {
        throw createError(404, { code: 404, message: 'Conta não encontrada' });
      }

      return accountModel.get().balance;
    } catch (error) {
      throw error;
    }
  }

  async updatePassword(accountNumber, password) {
    try {
      const accountModel = await Account.findByPk(accountNumber);

      if (!accountModel) {
        throw createError(404, { code: 404, message: 'Conta não encontrada' });
      }

      accountModel.password = password;

      await accountModel.save();

      await accountModel.reload({ include: ['owner'] });

      return accountModel.get();
    } catch (error) {
      throw error;
    }
  }

  async addFavored(accountNumber, favoredAccountNumber) {
    try {
      const [favoredAccountModel, accountModel] = await Promise.all([
        Account.findByPk(favoredAccountNumber),
        Account.findByPk(accountNumber)
      ]);

      await accountModel.addFavored(favoredAccountModel);

      return { added: true };
    } catch (error) {
      throw error;
    }
  }

  async removeFavored(accountNumber, favoredAccountNumber) {
    try {
      const [favoredAccountModel, accountModel] = await Promise.all([
        Account.findByPk(favoredAccountNumber),
        Account.findByPk(accountNumber)
      ]);

      await accountModel.removeFavored(favoredAccountModel);

      return { removed: true };
    } catch (error) {
      throw error;
    }
  }
}

module.exports = AccountController;
