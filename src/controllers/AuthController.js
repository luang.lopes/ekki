const bcrypt = require('bcrypt');
const createError = require('http-errors');
const tokenService = require('../services/tokenService');
const { Account } = require('../models');

class AuthController {
  async verifyCredentials(accountNumber, password) {
    try {
      const accountModel = await Account.scope('withPassword').findByPk(
        accountNumber,
        { attributes: ['password'] }
      );

      if (!accountModel) {
        return false;
      }

      const passwordMatch = bcrypt.compare(password, accountModel.password);

      return passwordMatch;
    } catch (error) {
      throw error;
    }
  }

  async authenticate(accountNumber, passowrd) {
    try {
      const verifyed = await this.verifyCredentials(accountNumber, passowrd);

      if (!verifyed) {
        throw createError(400, {
          code: 400,
          message: 'Conta ou senha incorretos'
        });
      }

      const token = await tokenService.createToken(accountNumber);

      const accountModel = await Account.findByPk(accountNumber, {
        include: ['owner']
      });

      return { token, account: accountModel.get() };
    } catch (error) {
      throw error;
    }
  }

  async verify(token) {
    try {
      const decoded = await tokenService.verifyToken(token);

      if (!decoded) {
        return { valid: false };
      }

      const accountModel = await Account.findByPk(decoded.accountNumber, {
        include: ['owner']
      });

      if (!accountModel) {
        return { valid: false };
      }

      return { valid: true, account: accountModel.get(), token };
    } catch (error) {
      throw error;
    }
  }
}

module.exports = AuthController;
